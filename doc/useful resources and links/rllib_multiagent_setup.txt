Doc sobre configurar un MultiAgentEnv en RLLib

https://ray.readthedocs.io/en/latest/rllib-env.html#multi-agent-and-hierarchical

Un par de Issues interesantes en GitHub sobre rollout en multiagent:
- https://github.com/ray-project/ray/issues/7659
- https://github.com/ray-project/ray/issues/7660
