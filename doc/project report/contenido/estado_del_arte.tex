\chapter{State of the art}
\label{chap:estado_del_arte}

\lettrine{O}{ne} problem of humankind is individualism.

\quad We all have certain tendency to be self-centered and rely solely on our capabilities, focusing on the outcome of our work instead of the accomplishments of others. However, the only way to break our limits is to do it together. \emph{Four eyes see more than two}, and seven billion minds think more than one, indeed.

In this chapter we will be commenting the most up-to-date reinforcement learning techniques and frameworks. 

For further information about them in relation to this project, custom trials run and other characteristics, see Chapter~\ref{chap:resultados}.

\section{Reinforcement learning algorithms}
\label{sec:rlalgorithms}

\subsection{Deep Q-learning (DQN)}
\label{sec:dqn}

Q-learning is the classical approach to reinforcement learning problems. It consists on building a table with an entry for each state-action pair, holding its Q-value, which is the total expected reward after taking such action at the given state.
\begin{equation*}
	Q(s, a)
\end{equation*}

However, this reward depends on future states and future actions, so it cannot be directly inferred.
\begin{equation*}
	Q(s_t, a_t) = \gamma \cdot Q(s_{t+1}, a_{t+1}) + \gamma^2 \cdot Q(s_{t+2}, a_{t+2}) + \cdots
\end{equation*}

This problem can be solved by applying the Bellman's equation \cite{JustinFrancisQLearningDQN}, which will update the Q-value in the table iteratively until each entry in the table converges to the optimal value. This update takes place every time the agent interacts with the environment.
\begin{equationcap}{Q-learning formula}{fig:qlearningformula}
	Q_{t+1}(s_t, a_t) = \underbrace{Q_t(s_t, a_t)}_{old\ value} + \underbrace{\alpha}_{\substack{learning \\ rate}} \cdot [ \overbrace{\underbrace{R_{t+1}}_{reward} + \underbrace{\gamma}_{\substack{discount \\ factor}} \cdot \underbrace{\underset{a}{max}Q_t(s_{t+1}, a_{t+1})}_{\substack{estimated\ \\ optimal\ future\ value}}}^{learned\ value} - \underbrace{Q_t(s_t, a_t)}_{old\ value}]
\end{equationcap}

The most convoluted part of the formula is the \emph{estimated optimal future value}, which consists on the maximum Q-value associated with the action for the state under evaluation. It can be seen as looking up in the table the action with the highest Q-value for the state in which the agent happens to be, and then retrieving the corresponding Q-value. Also, the agent will take such action in the next step, because it is optimal according to the current Q-table.

Although the theory is promising, it does not scale well. As environments grow in complexity the amount of possible states becomes unhandleable. 

How do we solve this?

There is a paradigm in machine learning called \emph{deep learning}. ``Deep learning is a class of machine learning algorithms that uses multiple layers to progressively extract higher level features from the raw input \cite{SIG-039}.'' Modern deep learning models achieve this by using artificial neural networks (ANN), although deep learning has greatly evolved in last decades due to the possibility of processing larger amounts of data.

The main idea behind deep Q-learning is to use an ANN as a function aproximator for the Q-value associated with each possible action for a given state.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.85]{imagenes/diagrams/dqn_network.pdf}
    \caption{Deep Q-learning basic architecture}
    \label{fig:dqnnetwork}
\end{figure}

In deep Q-learning, we train our agent by training a neural network instead of updating the Q values of a table. But before sailing any further, let us not forget about the basics. How does this training really take place?

\subsubsection{Let yourself roll down the hill}
\label{subsubsec:pg}

\emph{Gradient descent} is a mathematical iterative algorithm for finding the minimum of a function. It consists on taking steps proportional to the \emph{negative} of the gradient of the function, what implies that such function needs to be differentiable. This method was proposed by Cauchy in 1847 \cite{CauchyGradient}.


\begin{figure}[h!]\centering
    \includegraphics[scale=0.25]{imagenes/graphics/gradient_preexample}
    \caption{Graphical representation of $f(x) = 2x^3 - 5x^2$}
    \label{fig:gradient_preexample}
\end{figure}


\begin{figure}[h!]

\lstset{language=Python}

\begin{lstlisting}
  next_x = 5 # Start the search at x=5
  gamma = 0.02 # Step size multiplier
  precision = 0.00001
  max_iters = 10000

  # Derivative function
  def df(x):
    return 6 * x ** 2 - 10 * x

  for _ in range(max_iters):
    current_x = next_x
    next_x = current_x - gamma * df(current_x)
    step = next_x - current_x
    if abs(step) <= precision:
      break

  print("Minimum at ", next_x)
  # "Minimum at 1.666701170799059"
\end{lstlisting}

\caption{Python implementation of gradient descent for $f(x) = 2x^3 - 5x^2$}
  \label{fig:pythongradient}
\end{figure}

Artificial neural networks produce different outputs depending on their weights, which are usually denoted as a parameter $\theta$.

So, same as we found that for $f(x) = 2x^3 - 5x^2$ there is a local minimum near $x=1.667$, we want to discover which parameter value $\theta$ corresponds to the network producing the \emph{most accurate} output values.

We do not use the derivative function, because it is not known. Instead, a loss function is used, consisting on the mean squared error of the predicted Q-value and the target optimal Q-value. It is important to notice the similarities between equations \ref{fig:qlearningformula} and \ref{fig:dqnloss}. However, whereas $Q_t$ refers to looking up the Q-value inside a table at moment $t$, $Q_{\theta_{t}}$ refers to the output Q-value generated by an ANN parameterized by $\theta$ at moment $t$. 

\begin{equationcap}{Loss for training ANN in deep Q-learning}{fig:dqnloss}
	Loss = [R + \gamma \cdot \underset{a}{max}Q_{\theta_{t}}(s_{t+1}, a_{t+1}) - Q_{\theta_{t}}(s_t, a_t)]^2
\end{equationcap}

Again, we are dealing with an unknown optimal Q-value, but ``since \(R\) is the unbiased true reward, the network is going to update its gradient [\ldots] to finally converge \cite{AnkitHandsOnDQN}.''

\subsubsection{Target network and prioritized experience replay}
\label{subsubsec:targetnetworkandexperiencereplay}

The main difference between Q-learning and deep Q-learning is that an \emph{exact} value function is replaced with a function estimator (an ANN). But this means that instead of updating just \emph{one} state-action pair per timestep, a change in the neural network might be updating \emph{many}. Sometimes this translates into the effect of \emph{catastrophic forgetting}, which can make the agent suddenly start performing exceptionally bad after being learning progressively for a while.

Why does this happen?

In a classical deep learning problem, the target to train the network stays the same. For example, if an ANN is trained to recognize elephants in images, the target dataset does not change during the whole training. This is not the case for deep reinforcement learning. During training, the neural network is pursuing a target that is constantly changing, and since the same network calculates the predicted Q-value and the target Q-value, it is difficult to make the training stable.

One proposed solution to address this issue is using a target network \cite{TargetNetworkExpReplayMnih}. Such architecture would consist on having a copy of the Q-network with frozen weights and use it for estimating the target. Then, periodically, the weights of the Q-network would be updated on it. ``This leads to more stable training because it keeps the target function fixed (for a while) \cite{AnkitHandsOnDQN}.''

\begin{figure}[h!]\centering
    \includegraphics[scale=0.85]{imagenes/diagrams/dqn_target_network.pdf}
    \caption{Deep Q-learning architecture with target network}
    \label{fig:dqntargetnetwork}
\end{figure}

Another problem that arises in deep reinforcement learning is sampling efficiency, which means that samples obtained from the environment might be bringing poorly representative information and therefore affecting the training process negatively. This happens because in some environments the real complexity of the problem can reside only on certain states, and at some points the actions do not affect the environment in meaningful ways.

Prioritized experience replay attempts to improve this. Instead of directly running the learning process of the network for state-action pairs as they occur during the simulation, we allocate a large table that holds tuples of \emph{[state, action, reward, next state]} \cite{TargetNetworkExpReplayMnih}. Later on, different strategies can be used to decide a subset of those states which we find more suitable in order to feed the training process, and use them. 

\subsubsection{Double deep Q-learning (DDQN)}
\label{subsubsec:ddqn}

Using a neural network for estimating the Q-value is great — more concretely, £400 million great \cite{GoogleDeepMindBuy}. But more improvements were to come after Google bought UK artificial intelligence startup Deepmind.

Under certain conditions deep Q-learning networks tend to be \emph{overoptimistic}. For instance, an agent that learns to play a racing game might get some high initial rewards when turning left and consequently never choose to turn right. In both Q-learning and deep Q-learning, such \emph{overoptimistic} behaviour can be blamed on the \emph{max} operator. As discussed in Figure~\ref{fig:qlearningformula}, this operator (referred to as \emph{estimated optimal future value}) is used to both update the Q-network and select the next action to take.

The idea behind DDQN is to ``decompose the \emph{max} operation in the target into action selection and action evaluation \cite{GoogleDeepMindDDQN}.'' Simply put, the stable target network is used to estimate the Q-network, which remains used for evaluating the next action. Doing so, effectively reduces \emph{overoptimism}.

As a side note, Double deep Q-learning (DDQN) is not to be confused with double Q-learning, which appeared five years earlier and dwells on the same principle. However double Q-learning was originated to improve the quality of a simple tabular Q-learning algorithm \cite{NIPS2010_3964}.

\subsubsection{Dueling network architecture}
\label{subsubsec:dueling}

Although DDQN algorithm demonstrated state-of-the-art performance in Atari 2600 domain \cite{GoogleDeepMindDDQN}, this record was quickly beaten by the dueling network architecture \cite{DBLP:journals/corr/WangFL15}.

Firstly, let us have a quick overview of some fundamental concepts for the dueling network architecture. As it has been explained, the Q-value represents the value of choosing a specific action at a specific state. Another important term is the $V$ value, which corresponds to the value of a certain state regardless of the possible actions. Finally, as its name reflects, the advantage value, $A(s, a)$, represents how \emph{good} it is to select one action in comparison to others, for a given state \cite{ChrisYoonDuelingQ}.
\begin{equation*}
	A(s, a) = Q(s, a) - V(s)
\end{equation*}

The dueling architecture splits the network into two streams. One estimates $V(s)$ and other estimates $A(s,a)$. Thanks to this, the agent can learn which states are most valuable without having to learn the effect of each action for each state.

%\subsection{Advantage Actor-Critic (A2C, A3C)}
%\label{subsec:aac}

%To-Do

%https://ray.readthedocs.io/en/latest/rllib-algorithms.html#advantage-actor-critic-a2c-a3c

%We propose a conceptually simple and lightweight framework for deep reinforcement learning that uses asynchronous gradient descent for optimization of deep neural network controllers. We present asynchronous variants of four standard reinforcement learning algorithms and show that parallel actor-learners have a stabilizing effect on training allowing all four methods to successfully train neural network controllers


\subsection{Policy gradients (PG)}
\label{subsec:pg}

As it has been explained, in deep Q-learning the focus dwells on training a function approximator in order to get the proper Q-values for each state-action pair. Then, the agent usually plays a greedy policy\footnote{A \emph{policy} ($\pi$) is a key concept in reinforcement learning. It represents a \emph{way of behaving}. For a state $s$ and an action $a$ it outputs the possibility of taking such action $\pi(s, a)$. Due to this, it is frequently interpreted as a function that maps states to actions $\pi(s)=a$.} and simply selects the action with higher expected reward. Policy gradients algorithms \cite{PGAlgorithmsLilianWeng} are another large family of algorithms that go a step further — they aim to directly train the policy of the agent.

Why policy gradients?

Q-value function estimation has several limitations. ``First, it is oriented toward finding deterministic policies, whereas the optimal policy is often stochastic\footnote{Deterministic policies output a well defined action for the agent, while stochastic policies output the probabilities of taking each action. Then, the actual action is sampled from the probability distribution provided by the policy.}. Second, an arbitrarily small change in the estimated value of an action can cause it to be, or not to be, selected. Such discontinuous changes have been identified as a key obstacle to establish convergence assurances \cite{PGSutton}.'' 

\subsubsection{Optimize the policy \emph{directly}}
\label{subsubsec:opd}

Policy gradient algorithms attempt to optimize the policy of the agent, which is usually represented as a parameterized function respect to $\theta$, this is, $\pi_{\theta}(s, a)$. As mentioned earlier in this chapter, when talking about $\theta$ we are normally referring to the weights of artificial neurons in an ANN, as that is usually the approach used for implementing the policy of the agent. 

So, same as we used a loss function to train the ANN in equation \ref{fig:dqnloss} we will now define a reward function $J(\theta)$ so that we will find the $\theta$ value for which the reward of $\pi$ is maximum. Instead of using gradient descent, we will be speaking about gradient ascent because we want to get higher rewards. Thus, we will seek for the steepest change in the positive direction. 

The reward function for which we will need to compute the gradient is defined as follows \cite{PGAlgorithmsLilianWeng}:

\begin{equationcap}{Reward function for policy optimization using policy gradient}{fig:rewardfunctionpg}
	J(\theta) = \sum_s d^{\pi_{\theta}}(s) \cdot V^{\pi_{\theta}}(s) = \sum_s \underbrace{d^{\pi_{\theta}}(s)}_{\substack{state \\ distribution}} \cdot \sum_a \underbrace{\pi_{\theta}(s, a)}_{\substack{action \\ distribution}} \cdot \underbrace{Q^{\pi_{\theta}}(s, a)}_{\substack{expected \\ reward}}
\end{equationcap}

Note that, while in deep Q-learning algorithms a function approximator (an ANN) is trained in order to estimate the expected rewards $Q(s, a)$, here the idea appears to be more complicated.

The equation \ref{fig:rewardfunctionpg} says that given some policy parameter $\theta$, the reward for the policy corresponds to the sum of the \emph{expected reward} of each action that can be taken at any state that the agent acting under such policy happens to be.

First, what does the \emph{state distribution} represent?

Let us imagine a simple environment with three states. The agent can choose to move right whenever in $s_1$ or $s_2$, and to move left whenever in $s_2$ or $s_3$. Also, for every state, agent can choose not to move at all.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.85]{imagenes/diagrams/stationary_dist_example.pdf}
\end{figure}

Then, regardless of the starting state, if the agent travels forever the probability of ending up in a certain state remains unchanged. This is the main reason why PageRank works and is used by many search engines.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.8]{imagenes/diagrams/stationary_dist_example_policies.pdf}
    \caption{Example of different state distributions depending on agent policies}
    \label{fig:statesdistspolicies}
\end{figure}

We can now say that the \emph{state distribution} represents the probability of our agent ending up in a given state. However, as shown in Figure~\ref{fig:statesdistspolicies}, the policy affects the state distribution of the environment. If we tried to solve the \emph{gradient ascent} computation for $J(\theta)$, we would find that we do not know \emph{how} the changes in the policy affect the state distribution, because the environment is a black box function.  

Policy gradient theorem \cite{PGSutton} will help us with that, because ``it provides an analytic expression of the gradient of $J(\theta)$ that does not involve differentiation of state distribution \cite{SimoniniThomasPG}.''

\begin{equationcap}{Policy gradient theorem}{fig:policygradienttheorem}
	\nabla_{\theta}J(\theta) = \nabla_{\theta} \sum_s d^{\pi_{\theta}}(s) \cdot \sum_a \pi_{\theta}(s, a) \cdot Q^{\pi_{\theta}}(s, a) \overbrace{\propto \sum_s d^{\pi_{\theta}}(s) \cdot \sum_a \nabla_{\theta} \pi_{\theta}(s, a) \cdot Q^{\pi_{\theta}}(s, a)}^{score\ function\ gradient}
\end{equationcap}

Thus, computing the gradient of the reward function $\nabla_{\theta}J(\theta)$ is simplified a lot, because it only involves the gradient of $\pi_\theta$, and not the gradient of the state distribution. Although the problem of having a proper function estimator for the expected reward $Q(s, a)$ still remains, the policy gradient theorem is the basis for many reinforcement learning algorithms that no longer aim to calculate a value function, but to train the agent policy directly. 

\subsection{Proximal policy optimization (PPO)}
\label{subsec:ppo}

According to OpenAI, ``getting good results via policy gradient methods is challenging because they are sensitive to the choice of stepsize — too small, and progress is hopelessly slow; too large and the signal is overwhelmed by the noise, or one might see catastrophic drops in performance. They also often have very poor sample efficiency, taking millions (or billions) of timesteps to learn simple tasks \cite{PPOWebOpenAI}.''

And what did they propose in order to overcome such limitations?

\subsubsection{Walking the path with little steps}
\label{subsubsec:walkingthepathwithlittlesteps}

``Proximal policy optimization (PPO) strikes a balance between ease of implementation, sample complexity, and ease of tuning, trying to compute an update at each step that minimizes the cost function while ensuring the deviation from the previous policy is relatively small \cite{PPOWebOpenAI}.''

So, the basic idea in PPO is that policy updates will be \emph{clipped}. This way, we ensure that after each policy update the new policy does not deviate too far from the old one. We will be talking about a loss function $L(\theta)$ instead of a score function $J(\theta)$. Furthermore, the optimization will not target the loss function itself but a \emph{surrogate clipped loss function}. 

\begin{equationcap}{Surrogate clipped loss function}{fig:lclipppo}
	L^{CLIP}(\theta) = \hat{E}_t[{min}(r_t(\theta)\hat{A}_t,\ {clip}(r_t(\theta), 1 - \epsilon, 1 + \epsilon)\hat{A}_t)]
\end{equationcap}

A key concept for understanding this equation is the probability ratio $r_t(\theta) = \frac{\pi_\theta(s,a)}{\pi_{\theta_{old}}(s,a)}$ which is a way of measuring how far the new policy is from the old one. In PPO we will use an hyperparameter $\epsilon$ that will force the policy to stay within an interval for each policy update. If the advantage $\hat{A}_t$ for a training step is positive, the policy parameters will never be updated so that $r_t$ goes beyond $1 + \epsilon$. If the advantage $\hat{A}_t$ is negative, it will never fall below $1 - \epsilon$.



% maybe attribute image source of gradient circles By Gradient_descent.png: The original uploader was Olegalexandrov at English Wikipedia.derivative work: Zerodamage - This file was derived from:  Gradient descent.png:, Public Domain, https://commons.wikimedia.org/w/index.php?curid=20569355By Gradient_descent.png: The original uploader was Olegalexandrov at English Wikipedia.derivative work: Zerodamage - This file was derived from:  Gradient descent.png:, Public Domain, https://commons.wikimedia.org/w/index.php?curid=20569355

%https://ray.readthedocs.io/en/latest/rllib-algorithms.html#proximal-policy-optimization-ppo
%https://openai.com/blog/openai-baselines-ppo/#ppo
%https://openai.com/blog/emergent-tool-use

%Nice-to-have: Talk also about this (another part of the paper)
%\begin{figure}[h!]
%\begin{center}
%\noindent\fbox{
%  \parbox{.8\textwidth}{
%          \textbf{for} iteration=1, 2,\ \dots \ do
%
%          \quad \textbf{for} actor=1, 2,\ \dots \ $N$ do
%
%          \qquad Run policy $\pi_{\theta_{old}}$ in environment for $T$ timesteps
%
%          \qquad Compute advantage estimates $\hat{A}_1$\ \dots \ $\hat{A}_t$
%
%          \quad \textbf{end for}
%
%          \quad Optimize surrogate $L$ wrt $\theta$, with $K$ epochs and minibatch size $M \leqslant N T$
%
%          \quad $\theta_{old} \leftarrow \theta$
%
%          \textbf{end for}
%  }
%}
%        \caption{PPO algorithm, actor-critic style}
%  \label{fig:ppoalgorithm}
%\end{center}
%\end{figure}

\subsection{Other algorithms}
\label{subsec:otheralgorithms}

There are many other reinforcement learning algorithms that are relevant nowadays. Diving into all of them is not possible within the scope of this project. \textbf{Soft Actor Critic} \cite{haarnoja2018soft} aims to maximize not only the rewards but also the entropy of the policy. \textbf{Augmented Random Search} \cite{mania2018simple} is a random search method for training linear policies for continuous control problems. \textbf{Deep Deterministic Policy Gradient} \cite{lillicrap2015continuous} is an algorithm that learns both a Q function and a policy. And the list goes on\dots

%And the list continues\cdots

\section{Standardization efforts and most used frameworks}
\label{sec:standardization}

Reinforcement learning is a very promising branch of artificial intelligence. It has shown meaningful advances and many new techniques have arisen. This means that very different people are working and investing time on it, which makes it harder to unify all the work. In this section we will discuss several frameworks related to the topic and relevant within the context of this project.

\subsection{TensorFlow}
\label{subsec:tensorflow}

TensorFlow\footnote{{\scriptsize \url{https://www.tensorflow.org}}} is an open source library to help develop and train machine learning models. It has a wide range of tools that lets researchers push the state-of-the-art in machine learning.

It is a strong tool for creating and evaluating advanced artificial neural networks, and has many utilities that make it a core component in several artificial intelligence projects. It can be used with Python, JavaScript, C++ and Java.

\subsection{OpenAI Gym}
\label{subsec:openaigym}

Gym\footnote{{\scriptsize \url{https://gym.openai.com}}} is a toolkit for developing and comparing reinforcement learning algorithms \cite{OpenAIGym}. It provides a standard interface for the basic reinforcement learning problem definition (see Figure~\ref{fig:basicrlprob}) as well as a list of implemented environments and some agent examples.

OpenAI is an artificial intelligence research laboratory in San Francisco, California. Their stated aim is to promote and develop friendly AI in such a way as to benefit humanity as a whole.

\subsection{RLlib}
\label{subsec:rllib}

RLlib\footnote{{\scriptsize \url{https://github.com/ray-project/ray\#rllib-quick-start}}} \cite{liang2018rllib} is an open source library for reinforcement learning. It offers a huge variety of implemented algorithms and agent policies and high scalability (the different layers and components of RLlib\footnote{{\scriptsize \url{https://docs.ray.io/en/latest/rllib.html}}} are depicted in Figure~\ref{fig:rllibstack}). Even though RLlib is framework agnostic by design, it natively supports TensorFlow and PyTorch. 

RLlib runs on top of Ray\footnote{{\scriptsize \url{https://ray.io}}}, which is a framework for building and running distributed applications in Python.

\begin{figure}\centering
    \includegraphics[scale=0.6]{imagenes/graphics/rllib_stack.pdf}
	\caption[RLlib components]{RLlib components}
    \label{fig:rllibstack}
\end{figure}

\subsection{Other frameworks}
\label{subsec:otherframeworks}

\subsubsection{PyTorch}
\label{subsubsec:pytorch}

PyTorch\footnote{{\scriptsize \url{https://pytorch.org}}} is another open source machine learning framework. Same as TensorFlow, it views any model as a directed acyclic graph. However, while TensorFlow graphs must be defined statically before the model can run, the dataflow is way more imperative and dynamic in PyTorch. ``You can define, change and execute nodes as you go \cite{Pytorchvstf}.'' 

\subsubsection{Arcade Learning Environment (ALE)}
\label{subsubsec:ale}

Arcade Learning Environment\footnote{{\scriptsize \url{https://github.com/mgbellemare/Arcade-Learning-Environment}}} (ALE) is a simple framework that allows researches to build reinforcement learning agents for Atari 2600 games \cite{bellemare13arcade}. Measuring the performance of agents playing Atari games has become a common way of comparing and discussing how good a new algorithm is.

A recent article from Deepmind\footnote{{\scriptsize \url{https://deepmind.com/blog/article/Agent57-Outperforming-the-human-Atari-benchmark}}} reveals their work on an agent named Agent57. It relies on a meta controller that switches the type of algorithm to use, and is able to outperform human benchmarks for all the 57 games of the Atari suite. This is a recent breakthrough that has never happened before because agents that performed well on some games or in a wide range of games, did not manage to outperform humans in \emph{all} of them \cite{badia2020agent57}.

\subsubsection{PyGame Learning Environment (PLE)}
\label{subsubsec:ple}

PyGame Learning Environment\footnote{{\scriptsize \url{https://pygame-learning-environment.readthedocs.io/en/latest}}} is another similar framework for testing and measuring reinforcement learning agents that mimics ALE.

\section{Rendering tools and integration with machine learning problems}
\label{sec:rendering}

\subsection{Unreal Engine: Plugin for TensorFlow}
\label{subsec:tensorflow-ue}

Unreal Engine (UE) is a game engine that has become very popular in the last years. 

Unreal Engine: Plugin for TensorFlow\footnote{{\scriptsize \url{https://github.com/getnamo/tensorflow-ue4}}} is a plugin that enables training and implementing machine learning algorithms for UE projects. It acts as a wrapper for certain TensorFlow operations and allows them to run inside UE projects without the need for having TensorFlow models as a separate component.

\subsection{Unity3D: Machine learning}
\label{subsec:unity3d-ml}

Unity3D is another popular game engine nowadays.

Unity ML-agents\footnote{{\scriptsize \url{https://unity3d.com/machine-learning}}} is a toolkit that enables developers and researchers to use Unity projects as environments for machine learning experiments: intelligent agents can be trained and later used for multiple purposes.

\subsection{Godot}
\label{subsec:godot}

Godot\footnote{{\scriptsize \url{https://godotengine.org}}} is an open source game engine for 2D and 3D game development. It has an uprising community and many evolving features that are actively managed and developed.

We have chosen Godot as our graphics engine for creating an application, \emph{Reinforcement Learning Zombies}, that provides a visual output of the project results, allows interaction and enables us to reach a wider audience.

\subsection{Blender}
\label{subsec:blender}

Blender\footnote{{\scriptsize \url{https://www.blender.org}}} is an open source 3D creation suite. It is driven by a huge community and serves as a powerful tool for many projects. 

It used to provide a game engine for creating real time scenarios and interaction, but unluckily such component was removed from the project in 2018\footnote{{\scriptsize \url{https://developer.blender.org/rB159806140fd33e6ddab951c0f6f180cfbf927d38}}} due to obsolescence.
