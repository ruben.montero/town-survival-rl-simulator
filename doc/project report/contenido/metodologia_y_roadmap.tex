\chapter{Methodology, tools and roadmap}
\label{chap:metodologia_y_roadmap}

\lettrine{H}{ow}?

\quad That is a very important question.

For instance, one might wonder ``\emph{What} is that thing on the roof?'' 

``\emph{Why} is there a horse on the roof?''

But, most importantly, the real question is ``\emph{How} did a horse reach the roof?''

The core value of engineering is to create, understand and communicate \emph{how} things are done. In the previous chapters of this project report we have been dealing with \emph{why}, and in the following chapters the focus will be on \emph{what}.

Now, let us dive into \emph{how}.

\section{Methodology}
\label{sec:metodologia}

Choosing and sticking to an adequate methodology is key in almost every project. There are several software development methodologies that have been proven to work great, such as agile methodologies \cite{WhatIsAgile}, waterfall \cite{WaterfallModel}, or rapid application development \cite{RapidAppDev}. And I wish this project grows big enough so that one day the topic of ``which methodology to use'' is put on the table for a team of developers to discuss about it.

But that has not happened yet. This is still an individual project and the fundamental part of \emph{how} it is being done relies on self organization, appropriate documentation and guidance provided by the team of supervisors.

So, in this terms, the methodology for this project from the beginning to the present can be explained in two separated parts:
\begin{enumerate}
	\item \textbf{Initial stage}: During the first months of the project, the methodology for advancing towards a more solid state and a more well defined approach was similar to an incremental lifecycle. Basically, the main goal of this stage was to research about reinforcement learning algorithms and ``what'' and ``how much'' could be fitted within the scope of the project.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.825]{imagenes/diagrams/methodology_initial_stage.pdf}
    \caption{General phases of iterations during initial stage of the project}
    \label{fig:generaliterationphases}
\end{figure}

	The outputs of this phase iterations were discussed and effectively re-oriented to an appropriate path via e-mail threads and catch up meetings without a fixed schedule. It is to be understood that for each iteration, the real outcome might happen to be way smaller than the real effort behind it.  
\item \textbf{Mature stage}: We can consider that the project achieved a mature stage when the Godot integration was put in place and the RLlib framework usage was proposed. The methodology for this stage has been more similar to an agile approach. Fixed catch up meetings were scheduled every fortnight, in which most recent problems and results were discussed and goals for the following period were set up. That means a 2-week \emph{sprint} has been taking place since March, 2020.
	
	Main goals were also split into three milestones in which GitLab issues are added to:
	\begin{itemize}
	\item Improve custom Gym capabilities
	\item Test different agents (RLlib) and reach conclusions
	\item Integrate the simulation with a rendering engine
	\end{itemize}
\end{enumerate}

\section{Tools}
\label{sec:tools}

This project heavily relies on other libraries and projects that make it possible to keep our focus on the long run goals. These tools are:
\begin{itemize}
	\item \textbf{RLlib}: This powerful library provides several implementations of different reinforcement learning agents and allows us to focus on the environment design and implementation. Also, the benefit can be humbly considered mutual because we are testing several of the RLlib agent implementations, comparing and discussing the results.
	\item \textbf{Godot}: This is our main resource to achieve graphical output. In the very first meetings, the project proposal originally consisted on applying reinforcement learning techniques as a 3D rendering mechanism. That idea evolved, but the \emph{rendering} part prevailed as an attractive goal to be incorporated somehow into the project. Godot has been proven to be a very useful rendering engine and worth learning about. It is to be mentioned too that we have put a nice effort into using GDScript, a custom script language developed by the Godot community.
	\item \textbf{\LaTeX}: We are using \LaTeX\ for creating the project report. It has been generated departing from the project\footnote{{\scriptsize \url{https://git.fic.udc.es/laura.milagros.castro.souto/Modelo\_TFG}}} for unifying project reports of degree projects and master projects in the Computer Science Faculty of University of A Coruña.
	\item \textbf{GiLab}: We use GitLab for holding the Git repository associated with the project and present it to any future developer. Additionally, it is useful to keep tasks and milestones organized.
	\item \textbf{Python}: One of the most powerful programming languages and with very high potential for machine learning implementations. We use it to implement our custom Gym environment (see Chapter~\ref{chap:diseno_entorno_implementacion}), run the code for agent training and also for simulations rollout.
	\item \textbf{Tensorboard}: RLlib uses Tensorflow as a main component for several reinforcement learning agent implementations. This allows us to use Tensorboard for obtaining graphics about their performance and easily add custom metrics to be compared.
\end{itemize}

\section{Roadmap}
\label{sec:roadmap}

As formerly stated, the project can be divided into two main phases. The different types of workload for each of these two phases can be clearly observed in the plot of Figure~\ref{fig:commithistory}.
\begin{itemize}

	\item From August 2019 until March 2020, the research stage shows several spikes that belong to the different research iterations.

	\item From March 2020 on, the solid and consistent workload can be appreciated. In this phase, the main goal was to create the \emph{Reinforcement Learning Zombies} application and implement and run well defined experiments for the different agents.

\end{itemize}

\begin{figure}[h!]\centering
    \includegraphics[scale=0.28]{imagenes/graphics/commit_history}
    \caption{Number of commits per day in Git repository}
    \label{fig:commithistory}
\end{figure}

Almost every single one of the 200+ commits belonging to this project follows the guidelines specified in Section~\ref{sec:somedesignprinciples} and has an explanatory commit message that aims to stick to commonly accepted Git best practices.

For future work and next stages of our roadmap, refer to Chapter~\ref{chap:conclusiones}.
