\chapter{Introduction}
\label{chap:introduccion}

\lettrine{W}{e} all learn.

\quad But, what is \emph{learning}?

\vspace*{10pt}
\textbf{learning} (lɜːnɪŋ)

NOUN

\textbf{1.} knowledge gained by study; instructions or scholarship

\textbf{2.} the act of gaining knowledge

\textbf{3.} \textit{psychology} any relatively permanent change in behaviour that occurs as a direct result of experience

\vspace*{10pt}

Learning can be seen as the fuel for evolution. It enables us to achieve better results when facing different problems and is a mandatory tool for fulfilling our purposes in life. What is the purpose of life? Such question goes beyond the scope of this project, as we will try to keep it as simple as possible. We will not be focusing on human ability to learn. We will be focusing on computers.

In computer science, learning is a huge topic of research. Many years have passed since Arthur Samuel wrote a learning program in 1952 based on the game of checkers. It improved the more it played, incorporating moves that made up winning strategies \cite{ForbesShortHistoryML}.

Nowadays, artificial intelligence (AI) is being applied to a wide variety of fields from language processing to autonomously operating cars. More concretely, machine learning (ML) is considered a subset of artificial intelligence oriented to the study of algorithms that ``improve automatically through experience'' \cite{TomMitchelMLBook}, by ``building a mathematical model based on training data in order to make predictions or decisions without being explicitly programmed to do so \cite{BishopMLBook}.''

Are computers capable of learning, then?

There are several types of machine learning algorithms. ``Supervised learning algorithms build a mathematical model of a set of data that contains both the inputs and the desired outputs \cite{RusselStuartAIBook}.'' A good example of supervised learning is image classification. It starts by defining some kind of pattern that we want to identify in images, and trains a model to recognize it using a labeled dataset \cite{GoogleMLCourse}. This way we could make a computer automatically recognize cats inside pictures or help to diagnose cancer in clinical patients.

Another approach is unsupervised learning. ``This one differs from supervised learning in that only input data is known, but there are not corresponding output variables in the training set. The goal for unsupervised learning is to model the underlying structure or distribution in the data in order to learn more about the data. Unlike supervised learning, there are not correct answers or teacher. Algorithms are left to their own devises to discover and present the interesting structure in the data \cite{JBrownleeMLA}.''

In between, there is another area of machine learning called reinforcement learning.

\section{You can't learn without participating}
\label{sec:learnparticipating}

Reinforcement learning (RL) is an area of research focused on the key concepts of \emph{software agent} and \emph{environment}. An \emph{environment} is any representation of a task or scenario that has an internal state \(S_t\) given a precise instant in time. An \emph{agent} is a computer program of any nature capable of interacting with the environment.

How does this interaction take place?

Agents make choices among a set of possible actions contained in an \emph{action space}. The action is fed to the environment, and the environment responds providing an observation and a reward for the agent. If the observation represents the whole state of the environment we will be talking about \emph{full observability}. Otherwise, we speak of \emph{partial observability}. In anyway, the environment can be seen as a black box. We do not necessarily have a mathematical model for the environment, and the only way to learn about it is interacting with it. In this project, we will be considering such interactions to take place in discrete time steps.

``Reinforcement learning differs from supervised learning in not needing labeled input/output pairs to be presented, and in not needing sub-optimal actions to be explicitly corrected. Instead the focus is on finding a balance between exploration (of uncharted territory) and exploitation (of current knowledge) \cite{SurveyRL}.'' By sub-optimal actions we speak of decisions taken by the agent which are not the best choice for an arbitrary state at a given time step. But it is hard to tell whether playing a specific opening move will lead us to winning a chess game, right?

\section{But why bother to take an action?}
\label{sec:whybothertoact}

Getting up early on a Sunday to cook fried eggs for breakfast is not easy. But, in the same way, there is a reason why software agents take actions in an environment: the reward. This is another key concept in Reinforcement Learning. Every time the agent interacts with the environment, it gets back the observation of the new environmental state and a reward. 

\begin{figure}[h!]\centering
    \includegraphics[scale=0.85]{imagenes/diagrams/rl_basic_arch.pdf}
    \caption{Basic interaction in a reinforcement learning problem}
    \label{fig:basicrlprob}
\end{figure}

Broadly, the aim of the agent is to maximise the rewards it receives. ``The agent does not merely wish to maximise the immediate reward in the current state, but wishes to maximise the rewards it will receive over a period of future time \cite{WatkinsPHD}.''

There are three main methods of assessing future rewards that have been studied in the literature: total reward, average reward and total discounted reward. The total discounted reward from time \(t\) is defined to be

\begin{equationcap}{Discounted reward expression}{fig:discountedrewardsformula}
  r_t + \gamma r_{t+1} + \gamma^2 r_{t+2} \dots + \gamma^n r_{t+n} + \dots
\end{equationcap}
where \(r_k\) is the reward received at any time \(k\) and \(\gamma\) is a number between 0 and 1 (usually slightly less than 1). \(\gamma\) is termed the \textit{discount factor}. 

The effect of \(\gamma\) is to determine the present value of future rewards. If \(\gamma\) is set to zero, a reward at time \(t+1\) is considered to be worth nothing at time \(t\). If \(\gamma\) is set to be slightly less than one, then the total discounted reward from the current state will take into account expected rewards for some time into the future \cite{WatkinsPHD}.

To sum up, discount factor \(\gamma\) plays an important role in reinforcement learning as it manages the balance between learning from the short-term experience and weighting the impact of long-term rewards. This is strongly related to the exploration vs exploitation trade-off mentioned before. One cannot learn only from the immediate effect of their actions, but also, as it is said, \textit{in the long run we're all dead}.

\section{The world is there to be explored}
\label{sec:worldistobeexplored}

If we recap on the definition we gave at the beginning, reinforcement learning clearly has a strong relation with learning in nature, because software agents will modify their behaviour based on the experience obtained through interacting with the environment. In a world where many dimensions of our existence still remain unexplored and, in many cases, the only way to learn from an unknown context is to interact with it, reinforcement learning has huge potential.

Nowadays, this area of artificial intelligence has been successfully applied to a wide range of challenges such as the first successful autonomous completion on a real RC helicopter of four aerobatic maneuvers \cite{RCHelicopterPaper}, enough dexterity in a human-like robot hand to allow it to manipulate and solve a Rubik's cube \cite{OpenAIRobotHandPaper}, or the capability of robots to adapt to injuries or damaged parts in a similar way as an animal would do, without being limited to pre-specified self-sensing abilities and anticipated failure modes \cite{RobotsLikeAnimalsPaper}.

In this project we will explain different reinforcement learning methods and dive into them by creating a multiagent simulation. Some agents (\textit{citizens}) will compete against others (\textit{zombies}) in an environment similar to a hide-and-seek game. We will build an application that, in a didactic way, analyzes the performance and characteristics of different agents and training methods in this environment and also allows human interaction with it in order to actively compare and learn about the system.

Within the scope of this project, we will also try to reproduce or reach similar results as the ones obtained in OpenAI's hide-and-seek project, where a multiagent simulation revealed the capability of agents to develop six distinct strategies and counterstrategies by using tools inside the environment without having explicit incentives to do so \cite{EmergentToolUse}.

Additionally, we will integrate our simulation with a rendering engine for a better looking final result.

% Not sure about the following, maybe I don't do it so don't include it at the moment
%, and take the opportunity to consider the differences between having the visual context be part of the problem or just a layer on top of it.

