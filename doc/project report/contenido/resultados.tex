\chapter{Training results}
\label{chap:resultados}

\lettrine{S}{everal} trials have been run in order to obtain the \emph{best} training result. But, how do we determine which is \emph{best}?

In this chapter, we will be showing relevant metrics and comparing the performance of different state-of-the-art reinforcement learning algorithms implemented in the RLlib framework. Note that we will be focusing on the training of a \emph{zombie} agent, because that is a single task that will serve as a milestone for the comparisons.

For further information on the environment itself or related design decisions, refer to Chapter~\ref{chap:diseno_entorno_implementacion}.

\section{DQN}
\label{sec:dqn_performance}

We have chosen to train DQN \emph{zombies} because deep Q-learning is the most notorious reinforcement learning algorithm nowadays. It is the basis for many other methods and, also, several techniques have been designed around it.

In deep Q-learning the gamma $(\gamma)$ value, or discount factor, plays a special role. It configures the exploration vs exploitation tradeoff. The closer $\gamma$ is to $1$, the more relevance do future rewards have. When making it closer to $0$, only immediate actions are relevant during the training process.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_variable_gamma_qsrewards}
    \caption{Mean reward and mean Q for various gamma values in DQN}
    \label{fig:dqngammas}
\end{figure}

In Figure~\ref{fig:dqngammas} we can see the reward obtained by a DQN \emph{zombie} training against a \emph{citizen} that does not move, and thus, does not make the training unstable.

First of all, let us focus on how quickly the agent improves from a mean reward around $60$ per episode, same as a random agent would perform, to a greater mean reward. The training is stable during the first $200$ thousand timesteps, approximately. However, regardless of the $\gamma$ value, the agent never converges to a mean reward per episode greater than $150$.

In the right plot of Figure~\ref{fig:dqngammas} we can see the mean Q-value. This is not especially meaningful because as one would expect, having a greater discount factor makes the mean Q grow accordingly. However, it demonstrates that having a larger discount factor increases the variability of the Q-values during the training.

Although there is no clear winner on ``which $\gamma$ value is better'', Figure~\ref{fig:dqnrepduel} shows that dueling network architecture improves the training performance. The right graphic shows that prioritized replay does not bring any improved result. Both trials share the same discount factor, $\gamma = 0.95$.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_variable_dueling-replay}
    \caption{Mean reward for training using prioritized replay or dueling network in DQN}
    \label{fig:dqnrepduel}
\end{figure}

\section{PG}
\label{sec:pg_performance}

Policy gradient reinforcement learning algorithms are the base of the current state-of-the-art.

We have run a grid search over a set of different hyperparameters. We have used different \emph{learning rates} and \emph{batch sizes}. Note that there are many other hyperparameter search techniques and some of them are already implemented and provided in Tune \cite{liaw2018tune}, a project that belongs to the same repository as RLlib. However, we want to have complete trials for different hyperparameters values in order to discuss about them.

\begin{figure}[h!]\centering
		\includegraphics[scale=0.85]{imagenes/graphics/pg_variablelr}
		\caption{Mean reward for various learning rates in PG}
		\label{fig:pglr}
\end{figure}

In Figure~\ref{fig:pglr} we display training results for different \emph{learning rates}. For values of $0.0001$ and $0.0005$ the results improve slowly but in a stable way, whereas $0.001$ appears to learn more unstably.

\begin{figure}[h!]\centering
		\includegraphics[scale=0.85]{imagenes/graphics/pg_variablebs}
		\caption{Mean reward for various batch sizes in PG}
		\label{fig:pgbs}
\end{figure}

In Figure~\ref{fig:pgbs} we display training results for different \emph{batch sizes}.

\emph{Batch size} affects training meaningfully. A bigger \emph{batch size} means a bigger ``chunk'' of data to be fed into the neural network and processed. Therefore, more timesteps are needed in order to perform the same amount of training interations as \emph{batch size} grows, but the training is less noisy and is more likely to slowly converge to a maximum.

We can see that having a bigger \emph{batch size} makes the progress slower but more stable.

\section{PPO}
\label{sec:ppo_performance}

PPO is one of the most important reinforcement learning algorithms in the current state-of-the-art. It was used for training the agents in the OpenAI emergent tool use problem \cite{EmergentToolUse}

We have run a grid search over a set of different hyperparameters. We have used different \emph{learning rates}, \emph{clip params} and \emph{batch sizes}. 

\begin{figure}[h!]\centering
                \includegraphics[width=\textwidth]{imagenes/graphics/ppo_variable_lr_rewardentropy}
                \caption{Mean reward and entropy for various learning rates in PPO}
                \label{fig:ppolrs}
\end{figure}

In Figure~\ref{fig:ppolrs} we display training results for the same \emph{clip parameter value} $0.1$ and \emph{batch size} $8000$ while \emph{learning rate} changes. It can be seen that a smaller \emph{learning rate} benefits the training. In the right plot the entropy at different stages of the training is shown. We could say that this parameter measures how ``chaotic'' is the agent behaviour.  

\begin{figure}[h!]\centering
		\includegraphics[width=\textwidth]{imagenes/graphics/ppo_variable_cp_rewardentropy}
		\caption{Mean reward and entropy for various clip parameter values in PPO}
		\label{fig:ppocps}
\end{figure}

In Figure~\ref{fig:ppocps} we display training results for the same \emph{learning rate} $0.0001$ and \emph{batch size} $8000$ while the \emph{clip param} changes. This is a very relevant parameter specific to PPO, as smaller values ensure that new policy values do not deviate too much from previous ones, and thus, the training is limited. As we can see, a smaller value makes the training more stable.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/ppo_variable_batchsize_rewardentropy}
    \caption{Mean reward and entropy for various batch sizes PPO}
    \label{fig:ppobs}
\end{figure}

In Figure~\ref{fig:ppobs} we display training results for the same \emph{learning rate} $0.0001$ and \emph{clip parameter value} $0.1$ while \emph{batch size} changes. 


We can see that a training \emph{batch size} of $16000$ timesteps provides the best result for our PPO \emph{zombie}.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/ppo_klrewards_all}
    \caption{Mean reward and KL-divergence for all trials in PPO}
    \label{fig:ppokls}
\end{figure}

As we have seen, PPO agents reach higher reward values than DQN agents, although the training can be very different depending on hyperparameters. In Figure~\ref{fig:ppokls} we display the untagged graphics for all the aforementioned grid search trials.

The KL-divergence graphic shown at the right is especially useful, as it shows how much a policy strives away from its previous values. It can be seen how the \emph{clip parameter} affects the evolution of some trials, and how the trials run with bigger \emph{batch sizes} are more stable.


\section{Comparisons}
\label{sec:comparisons}

\begin{itemize}

	\item In general, we have seen that DQN is a powerful method, but also very unstable. It's more difficult to perceive the learning progress of the agent. However, the dueling network architecture effectively improves the quality of the algorithm. It still does not enable it to go up beyond the third position in our ranking in terms of ``maximum reward per episode stably achieved by chaser agent'', which means, how \emph{good} did our \emph{zombie} become.

	\item PG algorithm implementation of RLlib performs exceptionally good in our custom environment. The learning curve grows in a stable way. However, it takes longer to train and has not reached the same reward than the next algorithm.

	\item PPO has proven to achieve better training results in less iterations. It gets the better results for the \emph{zombie} and demonstrates that \emph{clipping} policy updates makes the policy evolve step by step.
\end{itemize}

\subsection{And what about the citizens?}
\label{subsec:whataboutcit}

Although the evaluation of the algorithms as a \emph{zombie} in our environment was successful, we were intrigued also by how a \emph{citizen}, training against a \emph{zombie} trained using the same policy, would perform.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/citizens_training}
	\caption{Citizen rewards for DQN, PG and PPO agents}
    \label{fig:citizenstraining}
\end{figure}

That is why we trained DQN, PG and PPO algorithms competing against their same type of algorithm, using the same hyperparameters as the ones that had achieved better reward as a \emph{zombie}\footnote{DQN: $gamma = 0.9$, $dueling=true$, $replay=false$. PG: $batch\ size=8000$, $learning\ rate=0.001$. PPO: $batch\ size=16000$, $clip\ param=0.1$, $learning\ rate=0.0001$.}. 

As we can see, the \emph{citizen} training for DQN is very unstable, although it seems to converge at the end. The PG performs a bit unstable too due to the \emph{learning rate} chosen, and PPO provides the more stable results. Also, to be noted, it seems that the \emph{citizen} that struggled the most during the \emph{zombie} training phase was the PPO one, although PG algorithm with smaller \emph{learning rates} obtained similar results. In spite of that, these graphics represent that DQN and PG are more vulnerable to training hyperparameters, while PPO is more robust. In any case, the three of them get higher rewards and are capable of learning to avoid their competitor and survive. 
