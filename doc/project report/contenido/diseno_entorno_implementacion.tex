\chapter{Environment design and implementation}
\label{chap:diseno_entorno_implementacion}

\lettrine{W}{hat} is the problem that we are trying to solve?

\quad That should be the first question to be asked before one starts working.

As specified in Chapter~\ref{chap:introduccion}, the main goal of this project is to dive into different reinforcement learning algorithms by creating a multiagent simulation and analyze the performance and characteristics of different agents and training methods. Reinforcement learning has been proven to perform well in many different domains. However, we do not want to apply reinforcement learning algorithms to solve predefined problems, but to analyze and understand the different reinforcement learning approaches in a hide and seek style environment. That is the main motivation to create a custom environment — it can be easily tuned to obtain insights about the performance of the agents.

In this chapter we will be describing the environment implemented and used in this project to train the different RL agents and run all the experiments.

\section{Some design principles}
\label{sec:somedesignprinciples}

Before jumping to the different phases of the environment development it would be appropriate to expose certain design principles that have been taken into account during the process:
\begin{itemize}
	\item \textbf{Everything should have a motivation}. Before writing a single line of code or choosing to use a certain framework instead of another, such change must be motivated meaningfully enough. Life is nothing but a fight and the only way to defend ourselves is with arguments. So in order to have arguments to defend one decision after it has been taken, those arguments must be known beforehand.
	\item \textbf{Best code is no code}. If something can be solved with 10 lines of code instead of 50, then that is probably a better solution. Every single line of code has the potential to contain bugs, so the process of debugging and understanding the code is easier if the code is simpler. Not only for the original author but also for other programmers. And we should see software as a tool open for anyone willing to give it a glance.
	\item \textbf{KISS} \emph{Keep it simple, stupid!} is another popular principle. Every time we add complexity to any system, we are forcing ourselves to put effort in the future to degrain it again. Simplicity is a virtue.
	\item \textbf{Whenever facing a problem, check if it has already been solved}. Reinventing the wheel is a common mistake in software development. It is desired to study if someone has already faced a similar problem and how it was solved, because that is the only way to improve as a community. If a direct solution can not be found, then it is desired to at least study the endeavour of others so that we do not end up taking the same wrong steps.
\end{itemize}

\section{Environment development history}
\label{sec:environmentdevelopmenthistory}

The environment itself has been conceived as a \scriptsize \texttt{git submodule} \normalsize inside the repository from the beginning of the project. The OpenAI Gym standard\footnote{{\scriptsize \url{http://gym.openai.com}}} has been used to define and implement it so that it could be easily integrated with other tools or frameworks.

\subsection{v0.0.1 (Initial version)}
\label{subsec:environment0.0.1}

The motivation for this version of the environment was to get in touch with reinforcement learning algorithms and OpenAI Gym. It was a simple 2D map where an agent aimed to reach a fixed goal point.

In this environment the main idea was to create an agent capable of walking through the most appropriate terrain in order to reach the goal as soon as possible. To be noted that the reward is modeled as a function directly related to the distance to the goal, which makes it easy for an agent to distinguish whether an immediate action is better or not. This was key for the Q-learning agent implemented to work properly.

The code documentation seen in Figure~\ref{fig:gymenvironment0.0.1} can be checked out in the submodule repository\footnote{{\scriptsize \url{http://gitlab.com/ruben.montero/gym-survival-multiagent}}} browsing the initial commits. It conforms to the OpenAI documentation style in the CartPole example\footnote{{\scriptsize \url{http://github.com/openai/gym/blob/master/gym/envs/classic\_control/cartpole.py}}}.

\lstset{language=make} % Just because it's a simple syntax and won't highlight keywords as for, var, etc.
\lstset{stringstyle=\color{black}}

\begin{figure}[h!]
\begin{lstlisting}
  Description:
    A citizen needs to move through a map to reach a goal point. Map layout is configurable, but it has some roads and mountains by default.

  Observation:
    Type: Box(2)
    Num Observation         Min    Max
    0   x position            0      1
    1   y position            0      1

    ^ y (+)
    |
    |
    |
    |-----> x (+)

    Note: Expressed in fractions of 1 where 1 is the maximum value of the canvas size.

  Actions:
    Type: Discrete(4)
    Num Action
    0   Move up
    1   Move right
    2   Move down
    3   Move left

    Note: The amount of space that the agent moves is not fixed. It depends on the terrain used to move.

    Mountain = 0.2 % of canvas (0.002)
    Grass    = 0.5 % of canvas (0.005)
    Road     = 2.0 % of canvas (0.020)

  Reward:
    Reward is a value [-1, 1], representing the difference between the Eulerian distance to the goal before taking the step and the Eulerian distance to the goal after taking the step.

  [...]
\end{lstlisting}
\caption{Description for custom Gym environment v0.0.1}
\label{fig:gymenvironment0.0.1}
\end{figure}

The map layout was also configurable and depending on the setup the agent might either converge to solving of the environment or early finishing an episode by jumping outside of the canvas. Note that in reinforcement learning an \emph{episode} consists on sucesive timesteps ended whenever the environment signals \scriptsize \texttt{done=true}\normalsize.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.2]{imagenes/screenshots/environment_alpha}
    \caption{Graphical representation of the environment v0.0.1}
    \label{fig:graphicalrepresentationalphaenvironment}
\end{figure}

In Figure~\ref{fig:graphicalrepresentationalphaenvironment} the output of the \scriptsize \texttt{render} \normalsize method can be seen. The agent is represented by a small red circle and the goal point by a dark green circle. The agent is able to find the optimal path using roads by applying the Bellman equation (\ref{fig:qlearningformula}) over a few thousand of episodes.
\subsection{v0.0.2 (Force-based movement)}
\label{subsec:environment0.0.2}

After this simple problem was successfully solved by a Q-learning agent, the following natural step was to add a small complexity to the environment and check if the good results prevailed. 

An intermediate 0.0.2 version was born with the main change of modifying the nature of the agent actions. Instead of directly updating its position, the agent would apply a force on itself and act following the Newton's laws of motion. The main motivation was getting closer to the OpenAI emergent tool use problem statement \cite{EmergentToolUse}. 

\subsection{v0.0.3 (Movable bridge)}
\label{subsec:environment0.0.3}

One of the main differences between 0.0.2 version and OpenAI emergent tool use project \cite{EmergentToolUse} was:
\begin{itemize}
	\item Interaction with objects in the environment.
\end{itemize}

Thus, in 0.0.3 version that was tackled. The main focus dwelled initially on allowing a single agent to interact with an object to reach the goal point. For this, an abyss was added in the middle of the map, as an horizontal bar that split the canvas into two halves. The agent could not step on the abyss. Its only chance to cross it was to move an object shaped like a rectangle that could be considered as a bridge. The goal point was located in the bottom half of the map.

This introduced several problems because the agent would often learn to move the bridge but it would get stuck in a middle point where it could not move forward and get to the other side of the abyss. This happened because the bridge was being moved a fixed distance but the agent applied a force on itself and reached a velocity higher than this amount per timestep.

To fix this, an attempt was carried out where the action space was changed from \scriptsize \texttt{Discrete(5)} \normalsize to \scriptsize \texttt{Discrete(9)}\normalsize, allowing the agent to choose to either move or push the bridge while moving. This was finally proven to obfuscate the training of the agent in a meaningful way.

Another problem faced during this stage was that the agent would normally move the bridge outside of the canvas and never get to use it appropriately. This was fixed by not allowing the bridge to exit the canvas, but it was proven that subtleties in the environment can make the same agent find the solution or converge to a local maximum of the Q-values where the environment solution is never reached.

\subsection{v0.1 (Multiagent approach with team-based rewards)}
\label{subsec:environment0.1}

As the experiments with the first versions of environment started growing, it was decided to keep the focus on the multiagent part of the system. For this, the different terrains of the environment were removed and the movable bridge idea was discarded. However, tackling the multiagent implementation of the environment was not trivial because Gym standard is limited to single agent environments and there are not extended conventions or standards for handling multiagent simulations. The reason for this is that depending on the problem statement the adequate implementation can vary.

\subsubsection{Multiagent support is not trivial}
\label{subsubsec:multiagentsupportnottrivial}

For example, in a board game environment one agent would act right after another. This means that changes in the environment performed by one agent directly affect the observation retrieved by the following agent, and this will happen sequentially during the whole episode. However, for other types of environments it might be desired to get all the actions at once and process them concurrently, providing a single observation each agent afterwards. This gets more complicated if we consider real time environments where actions and observations are transferred through a data stream instead of discrete steps, but that goes out of the scope of this project.

For our custom problem, we relied on the \scriptsize \texttt{MultiAgentEnv} \normalsize interface definition in the RLlib project. This handles the multiagent situation by receiving a dictionary holding all the actions in the \scriptsize \texttt{step} \normalsize function and returning a dictionary with the observations and rewards.

It was implemented in a \emph{wrapper class} in the main repository that would receive and return dictionaries with the information, but hold inside an instance of the custom Gym implementation and call its \scriptsize \texttt{step} \normalsize function sequentially passing an \scriptsize \texttt{agent\_index} \normalsize parameter. 

\subsubsection{Team-based rewards}
\label{subsubsec:teambasedrewards}

The most interesting part of this version was that we implemented team-based rewards.

\begin{figure}[h!]
\begin{lstlisting}
 Citizens are given a reward of +1 if all citizens are hidden and -1
 if any citizen is being caught by a zombie. Zombies are given the
 opposite reward, -1 if all citizens are hidden and +1 otherwise.

 During preparation phase, all agents are given 0 reward.
\end{lstlisting}
\caption{Rewards description for custom Gym environment v0.1}
\label{fig:gymdescriptionrewards0.1}
\end{figure}

\subsubsection{Where are the buildings in this town?}
\label{subsubsec:wherearethebuildings}

Another huge topic of discussion in this environment version was the obstacles introduced in the map layout. In order to reproduce a hide and seek game and get a closer approach to \cite{EmergentToolUse}, there should be objects that allowed some agents (the \emph{citizens}) to hide from others (the \emph{zombies}). Also, during this stage of the development the integration with the Godot engine was started (see Chapter~\ref{chap:render}).

%That is why from version 0.1 it is allowed to configure the map layout using a text file that represents walkable tiles with a ``\texttt{.}'' character, and non-walkable areas with a ``\texttt{x}'' character.

Additionally, the observation space was changed according to \cite{EmergentToolUse}, providing agents full awareness of the position of their team mates and awareness of the agents in the other team only if the distance to them is smaller than a certain configuration parameter. Another detail is that such awareness is occluded if an obstacle is in the line of sight, as seen in Figure~\ref{fig:graphicalrepresentationbetaenvironment}.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.4]{imagenes/screenshots/environment_beta}
    \caption{Graphical representation of the environment v0.1}
    \label{fig:graphicalrepresentationbetaenvironment}
\end{figure}

The environment was renamed from \scriptsize \texttt{gym-town-evolution} \normalsize to \scriptsize \texttt{gym-survival-multiagent} \normalsize and the older implementation was kept in a separated folder.

\subsection{v0.2 (Custom metrics and configurable parameters)}
\label{subsec:environment0.2}

After several experiments testing the RLlib agent implementations it was clear that discerning if agents really learn to hide and seek or to develop team strategies was not a trivial task.

With this in mind, a new version was implemented that returned custom metrics in the \scriptsize \texttt{info} \normalsize parameter returned by the \scriptsize \texttt{step} \normalsize function, in order to have additional information to measure the performance of the agents. This additional data consisted on:
\begin{itemize}

	\item The distance between agents allows us to tell if agents get closer to each other and what is the ratio for learning to get closer or escape from the enemy team.

	\item The tiles explored is a basic metric for understanding how much distance agents travel from their original location and it is especially useful when compared to a random baseline.

	\item The number of steps in a good hiding place are an elaborated metric intended for telling us whether \emph{citizens} learn to stay quiet in a place surrounded by walls where, as any human would judge, it is subjectively more difficult for the \emph{zombies} to find them.
		
		%Good hiding places are defined in the map layout text file with a ``\texttt{\_}'' character.S

\end{itemize}

\subsection{v0.3 (Agent-centric observation space)}
\label{subsec:environment0.3}

The results provided by the 0.2 version were not very promising. This was blamed on the nature of the observation space. Since the first version, positions were passed to the agents as absolute $(x, y)$ coordinates. This just does not play well with multiagent environments because the position $(0.35, 0.8)$ might represent something totally different depending on the positions and actions of the rest of agents. In Q-learning and deep Q-learning this easily prevents agents from converging to a reasonable output.

Empirically we also found that agents normally got stuck against walls, as they did not have any direct information about the non-walkable areas of the environment.

Once again, imitating the work in \cite{EmergentToolUse}, it was decided to change the observation space to relative distances from the point of view of the agent and add distance to walls as an attempt to emulate the lidar sensor presented there.

\subsubsection{It was hard for them to learn}
\label{subsubsec:itishardtolearn}

We trained two \emph{zombies} playing against two \emph{citizens}. Four RLlib \scriptsize \texttt{DQNTrainer} \normalsize objects were instantiated. After every training iteration, the weights of each agent were copied onto the rest of the networks in order to synchronise them.

This is simultaneous training as done in \cite{EmergentToolUse}.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_citizens_mean_reward}
    \caption{Mean reward for DQN citizens in environment v0.3}
    \label{fig:dqncitizensmeanreward}
\end{figure}

As we can see, compared to the performance of random agents, \emph{citizens} performed very similarly. Considering that there are 240 timesteps per episode and 96 correspond to a preparation phase in which all agents were given $0$ reward, the maximum possible reward was 144. 

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_zombies_mean_reward}
    \caption{Mean reward for DQN zombies in environment v0.3}
    \label{fig:dqnzombiesmeanreward}
\end{figure}

The outcome was similar for \emph{zombies}. After more than 2 million training timesteps, they still performed like random agents.

Rolling out the training results revealed that in some specific cases interesting behaviours were exhibited. However, agents did not really respond well to obstacles. Some videos can be found in issues in the project repository\footnote{{\scriptsize \url{https://gitlab.com/ruben.montero/town-survival-rl-simulator}}}, but anyway the performance of the agents was undeniable far away from the results we wanted to imitate from the OpenAI emergent tool use article \cite{EmergentToolUse}.

Why?

In an attempt to answer this question, we simplified the training. Firstly, we run the training with just one agent per team in order to prevent the Q-networks from getting biased by team-based rewards. Later on, in the 1v1 scenario we run the following experiments:
\begin{itemize}
	\item \textbf{No preparation phase}: The initial 96 timesteps where \emph{zombies} movement was restricted were removed.
	\item \textbf{Citizen position fixed}: The \emph{citizen} could not move.
	\item \textbf{No preparation phase and citizen position fixed}: Both conditions applied.
\end{itemize}

These scenarios were designed aiming to allow \emph{zombies} to ``win''. They have higher maximum speed by design, so it was desired that they ended up outplaying \emph{citizens}.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_extra_explored_tiles}
    \caption{Tiles explored in DQN 1v1 scenarios}
    \label{fig:dqnextratiles}
\end{figure}

As we can see in Figure~\ref{fig:dqnextratiles}, removing the preparation phase was a key factor that made the \emph{zombie} explore more as it learned. Although it was being granted $0$ reward, the decisions taken in that phase have empirically demonstrated to reduce convergence. Changes in favor of dueling network architecture or prioritized replay (see Section~\ref{subsubsec:targetnetworkandexperiencereplay}) did not exhibit better results.

\begin{figure}[h!]\centering
    \includegraphics[width=\textwidth]{imagenes/graphics/dqn_extra_distance_and_reward}
    \caption{Mean distance and zombie reward in DQN 1v1 scenarios}
    \label{fig:dqnextradistreward}
\end{figure}

Also, as seen in the left plot of Figure~\ref{fig:dqnextradistreward}, when there was no preparation phase, the mean distance between the two competitors was higher than in a random environment. We could interpret from it that the \emph{zombie} learned to chase the \emph{citizen}, but the \emph{citizen} also learned to avoid it. So, \emph{citizen} ``won'', what is also shown in the right plot. The \emph{zombie} began with very negative rewards, since it started getting $-1$ rewards earlier in the episode given the lack of a preparation phase. However, it learned that chasing the \emph{citizen} was good, so it did, and the \emph{citizen} learned to keep the distance around timestep 70k.

When the \emph{citizen} position was fixed, the reward obtained by the \emph{zombie} was almost identical to the random case, and the question still remains open on why the preparation phase drastically prevents the \emph{zombie} from getting positive rewards.

On top of that, we can say that when both conditions were implemented, the \emph{zombie} learned to effectively catch the \emph{citizen}. Actually, the \emph{zombie} just learned to pursue a fixed spot inside the map, similarly to our successful agent described in Section~\ref{subsec:environment0.0.1}. As commented in Section~\ref{subsubsec:targetnetworkandexperiencereplay}, having a moving target is a big problem in deep reinforcement learning because it makes training unstable. But this is unavoidable because of the black box nature of the environment function. However, if the \emph{citizen} position is fixed, it is, obviously, pretty easy for the \emph{zombie} to get some food.

\subsection{v1.0 (Individual rewards and no preparation phase)}
\label{subsec:environment1.0}

Under the light of the former results, our attempt to reproduce OpenAI emergent tool use was paused in favor of a simpler approach and it was decided that:

\begin{itemize}
\item Preparation phase should be removed.
\item Team-based rewards should be turned into single individual rewards.
\item Instead of training all agents at once, \emph{zombies} would be trained first, targeting fixed citizens. Later on, \emph{citizens} would learn by playing against already trained \emph{zombies}.
\end{itemize}

This allows us to really compare different RLlib implementations of reinforcement learning algorithms, as shown in Chapter~\ref{chap:resultados}.

Another key point is that observation space was made fixed. Instead of depending on the number of allies and enemies, in v1.0 it consists on a fixed \scriptsize \texttt{Box(7)} \normalsize that returns appropriate information about the closer enemy.

By today, the current version of the environment is 1.1.1, containing certain bugfixes and minor changes. It is fair to provide the code documentation (Figures~\ref{fig:gymdescriptionlast-1} and \ref{fig:gymdescriptionlast-2}) for the most recent version, as that fully reflects what is currently held in the repository and serves as a nice summary of this chapter.

\begin{figure}[h!]
\begin{lstlisting}
Description:
  N citizens escape from M zombies in a town.
  Each agent is represented as a pair (x,y) coordinates in a 2D map.

Source:
  Inspired in the OpenAI hide 'n' seek gym, that showed interesting
  agent behaviours about emerging tool use in multiagent
  environments: https://openai.com/blog/emergent-tool-use
  Here we don't implement team based rewards nor tool usage, though.

Observation:
  Type: Box(7)
  Num  Observation                            Min         Max
  0    top distance to closer wall             0           1
  1    right distance to closer wall           0           1
  2    bottom distance to closer wall          0           1
  3    left distance to closer wall            0           1
  4    agent speed                             0           1
  5    x distance to closer enemy team member -1           1
  6    y distance to closer enemy team member -1           1

  (I) x,y coordinates are interpretated left-right and top-bottom.

   ___ x (+)
  |
  |
  y (+)

  (II) agent speed (4) is relative to maximum speed.
       This is, mapped from [0, max_speed] to [0, 1].

  (III) Distances are calculated from the agent's perspective.
        Also, (5) and (6) are negative if target entity is to the
        left|top, and positive if it's right|bottom.

  (IV) If distance to closer enemy is > AWARENESS_RADIUS
       then observations (5) and (6) will be 1.

Actions:
  Type: Discrete(5)

  Num Action
  0   No-op
  1   Move up
  2   Move right
  3   Move down
  4   Move left
\end{lstlisting}
\caption{Description for custom Gym environment v1.1.1 (1 of 2)}
\label{fig:gymdescriptionlast-1}
\end{figure}

\begin{figure}[h!]
\begin{lstlisting}
Reward:
  - Each citizen receives +1 reward while not being caught (*) by a
    zombie and -1 otherwise.

  - Each zombie receives +1 reward while "catching" (*) a citizen.
    Otherwise, it receives (**):
    > [0, +0.75] reward depending on the distance to closer enemy
      team member. If very far away (> canvas width) it will be 0.
    > Additionally, a factor of [-0.25, 0.25] is summed, calculated
      from the agent's velocity. If zombie isn't moving, it gets
      -0.25 and if it moves at maximum velocity, +0.25.

  - During preparation phase, all agents are given 0 reward.

  (*) To consider that a citizen is being caught:
  - Distance between hider and seeker < CATCH_DISTANCE.
  - Line of sight isn't obscured by non-walkable areas between them.

  (**) These intrinsicly motivates zombie to explore and move
       towards citizen, improving convergence.

Starting State:
  Random positions for all agents, inside the walkable area.
  If PREP_PHASE_STEPS > 0, during preparation phase zombies can't
  move to give citizens a chance to hide.

Episode Termination:
  Fixed episode duration of EPISODE_STEPS steps.

Configuration:
  The following parameters can be passed during environment
  initialization. See @init method.

  Parameter         Default
  EPISODE_STEPS      240      Fixed episode length.
  PREP_PHASE_STEPS    0       Zombies can't move and reward is +0
  ACCELERATION        0.001   How rapid agents reach their maximum
                              speed. We use explicit integration.
  CATCH_DISTANCE      0.1     Default: 10% canvas width.
  AWARENESS_RADIUS    1       If small, observations (5) and (6) will
                              be 1 until agents are that close.
  CITIZEN_MAX_SPEED   0.005   Fraction of canvas width / timestep.
  ZOMBIE_MAX_SPEED    0.01    Fraction of canvas width / timestep.
  N_CITIZENS          2       Number of citizens.
  N_ZOMBIES           2       Number of zombies.
\end{lstlisting}
\caption{Description for custom Gym environment v1.1.1 (2 of 2)}
\label{fig:gymdescriptionlast-2}
\end{figure}
