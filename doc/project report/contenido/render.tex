\chapter{UX design and integration with rendering engine}
\label{chap:render}

\lettrine{``T}{he} best big idea is only going to be as good as its implementation.''

\hspace*{\fill}— Jay Samit\footnote{Source: Post from Jul 31, 2016
  ({\scriptsize \url{https://twitter.com/jaysamit/status/759774654626164736}})}

This project was originally conceived as a research project with a single goal, which was to investigate and compare different reinforcement learning techniques. However, this goal was soon perceived as an opportunity to implement an application to present the results and reach a wider audience. Additionally, during the development process of this application a protocol has been designed and implemented in order to communicate the UI frontend and the simulation server backend. Such protocol is extensible and is intended to work on top of any Gym environment, supporting multiple instances per server and multiagent integration. This makes it exceptionally easy to test the performance of any reinforcement learning agent over a local or remote network and also allows human interaction with the environment and other agents, as discussed in Section~\ref{sec:communicationprotocol}.

The application \emph{Reinforcement Learning Zombies}, developed as part of this project, can be obtained for desktop environments in Windows\footnote{{\scriptsize \url{https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/windows}}}, Linux\footnote{{\scriptsize \url{https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/linux}}} and macOS\footnote{{\scriptsize \url{https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/macos}}}.

Issues, suggestions and contributions are always welcome in the project repository\footnote{{\scriptsize \url{https://gitlab.com/ruben.montero/town-survival-rl-simulator}}}. 

\section{Client application}
\label{sec:graphicalclientdevelopment}

As discussed in Section~\ref{subsec:godot}, Godot has been the rendering engine chosen to create the graphical client in this project. Such development is divided into two main phases. The first one consists on rendering the reinforcement learning rollouts as they are — various agents considered to be \emph{zombies} and \emph{citizens} simulating a survival environment. The second one relates to the user interface for the application menus and everything else presented in it.

\subsection{Development of a 2D map in Godot}
\label{subsec:developmentofsimulationrepresentations}

Software development should be understood as something evolutive and core features should be prioritized. The first step towards our goal was to implement the representation of our reinforcement learning rollouts.

Due to this, the \textbf{art} style for the simulation was the first topic of discussion. 

Initially, it was desired to implement a 3D world and have the agents modeled as meshes inside it. However, out custom Gym environment never really evolved from a 2D conception of the map layout, so a 3D render soon started to sound overkill.

After discovering the potential of a 2D simulation based on a tilemap, all efforts were headed into that direction. Using Godot tilemaps allows us to ``draw the layout by 'painting' the tiles onto a grid \cite{GodotTilemaps}.'' Also, the art type was defined and asset packs that matched it were searched for. Finally, it was decided to use a pair of asset packs licensed as Creative Commons which have been properly credited inside the application.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.18]{imagenes/screenshots/wip_assets}
    \caption{Work in progress of some adaptations to sprites in the asset packs used}
    \label{fig:wipgameassets}
\end{figure}

As seen in Figure~\ref{fig:wipgameassets}, some extra work was done in order to obtain a better looking final result.

The heavy lifting of the development of a 2D map was carried out once the fundamentals about this method were properly learned. We have to thank the official Godot documentation, examples and community for making this process simpler and faster. It took no more than a couple of weeks to complete an initial version of the graphical map.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.15]{imagenes/screenshots/wip_town_making1}
	\caption{Work in progress of our 2D map in Godot (1 of 2)}
    \label{fig:wiptownmaking1}
\end{figure}

\begin{figure}[h!]\centering
    \includegraphics[scale=0.15]{imagenes/screenshots/wip_town_making2}
	\caption{Work in progress of our 2D map in Godot (2 of 2)}
    \label{fig:wiptownmaking2}
\end{figure}

Some interesting facts that can be spotted are:
\begin{itemize}
	\item The 2D map consists on several grids ordered in different layers. Some of them are assigned a z-index higher than the agents sprites, and some others a lower one. That depends on whether it is desired that agents are rendered above the layer (e.g.: the front part of a building) or below it (e.g.: a roof).
	\item Map is fully surrounded by non walkable areas because we do not want the agents to walk away from the simulation zone.
	\item The good hiding places as described in Section~\ref{subsec:environment0.2} are the small corridor on the left, above the red and green cars; the nook in the top-right corner, to the right of the gray and green cars; and the areas surrounded by trees in the middle of the map. 
\end{itemize}

The final result of the process is shown in Figure~\ref{fig:godotmap}.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.37]{imagenes/screenshots/godot_map}
        \caption{Final result of the 2D map creation process}
    \label{fig:godotmap}
\end{figure}

Needlessly to be said, the buildings and other obstacles that can be found in the map correspond to non walkable areas in the custom Gym environment. 

\begin{figure}[h!]\centering
    \includegraphics[scale=0.18]{imagenes/screenshots/godot_map_nonwalkableareas}
        \caption{Our 2D map with non walkable areas highlighted as $X$ tiles}
    \label{fig:godotmapnonwalkable}
\end{figure}

The definition of the map layout as described in Section~\ref{subsec:environment0.1} was done after the tilemap was setup. In Figure~\ref{fig:godotmapnonwalkable} the tiles considered to be obstacles are coloured in an upper layer. These can be compared with the custom Gym environment layout, which was formerly presented in Figure~\ref{fig:graphicalrepresentationbetaenvironment}.

Because of how areas in Figure~\ref{fig:godotmapnonwalkable} are marked, they can be easily translated to a raw text file that is then read and interpreted by our custom Gym environment implementation. Thanks to this method of integration, any change in the map layout of the rendering client can be easily translated to the reinforcement learning environment logic, and vice versa.

\subsection{User interface}
\label{subsec:uxrequirementsdesign}

The current \emph{Reinforcement Learning Zombies} application design can be considered the evolution from a \emph{beta} stage.

%\begin{itemize}
%	\item The application should present a startup wizard.
%	\item The application should briefly display some training results carried out during this project.
%	\item The application must allow participating in a lobby in order to join.
%	\item The application must allow joining simulations.
%%\end{itemize}

In Figure~\ref{fig:clientapp}, a couple of screenshots displaying the lobby and the main screen are shown. There is also a startup wizard and a screen for displaying training results. Note that we have chosen to stick to this ``minimalist'' design because it matches with the selected art style. 

\begin{figure}[h!]\centering
    \includegraphics[scale=0.24]{imagenes/graphics/clientapp}
        \caption{Screenshots of Reinforcement Learning Zombies desktop application}
    \label{fig:clientapp}
\end{figure}

\section{Server application}
\label{sec:serverdevelopment}

A simple server application has been implemented to host the simulations.

\begin{figure}[h!]\centering
    \includegraphics[scale=0.8]{imagenes/diagrams/repo_hierarchy.pdf}
        \caption{Overall structure of server code}
    \label{fig:overallserver}
\end{figure}

Figure~\ref{fig:overallserver} depicts the overall structure of the project's repository folder \scriptsize \texttt{python3}\normalsize. This directory contains the custom Gym environment as seen in Chapter~\ref{chap:diseno_entorno_implementacion}, and a couple of Command Line Interfaces (CLI). 

These can be used to launch server lobbies or to train agents (\emph{zombies} or \emph{citizens}) to be used later.

\begin{figure}[h!]
	\begin{lstlisting}[basicstyle=\scriptsize]
 usage: lobby_creator.py [-h] --lobby CHECKPOINT_PATH,N_CITIZENS,N_ZOMBIES
                         --port PORT

 Launch lobbies for 'Reinforcement Learning Zombies' client apps to register
 and participate in multiagent simulations. If you want to run this on a
 remote machine and leave it up after SSH logout, you might consider using
 'nohup your_command_script.sh &', as this program runs in a never-ending
 loop (alternatively, it could be launched into a terminal multiplexer,
 such as screen or tmux).
\end{lstlisting}
        \caption{Help output for {\tt \footnotesize lobby\_creator.py} CLI}
        \label{fig:helplobbycreatorpy}
\end{figure}

\begin{figure}[h!]
	\begin{lstlisting}[basicstyle=\scriptsize]
 usage: train.py [-h] --mode MODE --policy POLICY [--grid-search]
                 [--iterations ITERATIONS] [--checkpoint-dir CHECKPOINT_DIR]
                 [--zombie-checkpoint ZOMBIE_CHECKPOINT]
                 [--acceleration ACCELERATION]
                 [--awareness-radius AWARENESS_RADIUS]
                 [--catch-distance CATCH_DISTANCE]
                 [--citizen-max-speed CITIZEN_MAX_SPEED]
                 [--episode-steps EPISODE_STEPS]
                 [--prep-phase-steps PREP_PHASE_STEPS]
                 [--zombie-max-speed ZOMBIE_MAX_SPEED] [--dueling]
                 [--gamma GAMMA] [--prioritized-replay]
                 [--batch-size BATCH_SIZE] [--learning-rate LEARNING_RATE]
                 [--clip-param CLIP_PARAM] [--tau TAU] [--actor-lr ACTOR_LR]
                 [--critic-lr CRITIC_LR]

 Train an agent in order to be used in the hide'n'seek style gym environment.
 Intended usage consists on first training a zombie and obtaining checkpoints
 to be used for training a citizen afterwards. Training results can be viewed
 in Tensorboard if installed.
\end{lstlisting}
	\caption{Help output for {\tt \footnotesize train.py} CLI}
	\label{fig:helptrainpy}
\end{figure}


\section{Communication protocol}
\label{sec:communicationprotocol}

In order to communicate our custom Gym environment \scriptsize \texttt{step} \normalsize function with Godot, it was needed to connect the two ends somehow. A few ideas were weighted:

\begin{itemize}
	\item Usage of Linux pipes for unidirectional interprocess communication \cite{LinuxPipes}.
	\item Usage of shared memory for interprocess communication via system calls like \emph{ftok()}, \emph{shmget()}, \emph{shmat{}}, \emph{shmdt()}, \emph{shmctl()}.
	\item Usage of commonly written/read files managed through a filesystem.
\end{itemize}

The winning idea was to elaborate a custom simple protocol using \emph{UDP messages} to both obtain the information from the simulation server and send back the actions chosen by the client.

This has some fundamental advantages. It allows us to totally separate the rendering client from the environment logic so that those parts can be run in different machines. It is also platform independent, which makes it possible for any device running any operative system to act as a client for the Gym environment as long as it supports UDP/IP protocol. This is also a especially useful within the scope of our project because it is focused on multiagent environments.

A protocol baptised as \textbf{PERLERT} has been created for this purpose. It has been written conforming to RFC Style Guide \cite{rfc7322}, via ``xml2rfc'' version 2 vocabulary \cite{rfc7749}, which consists on a format definition for XML source that can be translated\footnote{{\scriptsize \url{http://xml2rfc.tools.ietf.org}}} into properly formatted ASCII RFC.

For further information on this protocol, refer to Appendix~\ref{chap:adicional}.
