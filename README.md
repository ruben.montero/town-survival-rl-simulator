**Town Survival RL Simulator** is a project consisting on a client and a server application that allows training and testing different reinforcement learning algorithm implementations in a [hide 'n' seek style environment](https://gitlab.com/ruben.montero/gym-survival-multiagent).

# Try it out!

The most straightforward way to get in touch with the project is to try out the client application, *Reinforcement Learning Zombies*.

You can choose your platform:

* *Reinforcement Learning Zombies* [for Windows](https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/windows)
* *Reinforcement Learning Zombies* [for macOS](https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/macos)
* *Reinforcement Learning Zombies* [for Linux](https://gitlab.com/ruben.montero/rlz-godot/-/tree/master/linux)

Once installed, you just need to run app and choose a server, port, and instance number. 

![demo](doc/reinforcement-learning-zombies-demo.mp4)

There is a list of [available public servers](doc/available_servers.txt). In any case, the intended usage is within a low latency environment, so you might choose to...

# ...run your own server

### Clone the repository:

```
git clone --recursive https://gitlab.com/ruben.montero/town-survival-rl-simulator.git
```

### Install the required dependencies

```
pip install -r requirements.txt
```

### Install the custom hide 'n' seek Gym environment

```
pip install -e code/python3/Libraries/gym-survival-multiagent/ --verbose
```

### ...and you're ready to go

Run the server application by using the `code/python3/lobby_creator.py` file. Multiple instances can be run simultaneously.

Python3 is required!

In example: (assuming you are in the directory `code/python3`)

```
python3 lobby_creator.py --lobby training/checkpoints/ppo,2,3 --port 23420
python3 lobby_creator.py --lobby training/checkpoints/pg,2,2 training/checkpoints/pg,1,1 --port 23420
```

These use pre-trained agents checkpoint files. There are some pre-trained agents located under the `training/checkpoints` folder. You can use them, or, alternatively, if you like to roll up your sleeves you can...

# ...train your own agents

```
python3 train.py --mode zombie --policy ppo --checkpoint-dir /home/my_user/zombie
python3 train.py --mode citizen --policy dqn --zombie-checkpoint /home/my_user/zombie/checkpoint_51/checkpoint-51 --checkpoint-dir /home/my_user/my-usable-checkpoint
```

The intended usage consists on:

1. Train a zombie agent and obtain checkpoints.
2. Train also a citizen using the output from the previous step.

There are several available options, including different policies (DQN, PG, PPO), hyperparameters and grid search. You can also add your own training code and try out different experiments, which reminds me of something important. You can...

# ...contribute!

If you want to raise a feature request, report a bug or simply get to know more about the project, simply [open a new issue](https://gitlab.com/ruben.montero/town-survival-rl-simulator/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## License

This project is Public Domain.

Please, see the attached [LICENSE file](LICENSE) for more information.

## Additional information

Communication between server and *Reinforcement Learning Zombies* application is carried out using the [PERLERT protocol](doc/perlert/perlert-01). If you are working on evaluating reinforcement learning environments that run in a separated machine, it might be worth taking a look at it.

This repository is part of a master's degree project of a University of A Coruña student, so if you feel that the previous information is still not enough, you can always read the complete [project report](doc/project report/project_report.pdf).
