"""
Rubén Montero Vázquez. MUEI UDC 2019/20

Utility class that opens checkpoints generated via RLLib checkpointing
and sets/adds/updates policy weights in them.

For instance, allows opening a checkpoint that holds information for:
- policy A: PPO TensorFlow trained policy
- policy B: Custom minimal policy (no ANN)

...and perform changes in it, i.e.:
- policy A: PPO TensorFlow trained policy
- policy B: Copy of policy A
- policy C: Newly initialized PPO TensorFlow policy (to be trained)
"""

from copy import deepcopy
from ray.rllib.agents.ppo.ppo_tf_policy import PPOTFPolicy
from ray.rllib.agents.pg.pg_tf_policy import PGTFPolicy
from ray.rllib.agents.dqn.dqn_tf_policy import DQNTFPolicy
from ray.rllib.utils.filter import NoFilter
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
import os
import pickle

class CheckpointAdapter():
  def __init__(self, obs_space, act_space, config = {}):
    self.paths = {}
    self.checkpoints = {}
    self.obs_space = obs_space
    self.act_space = act_space
    self.config = config

  """
  Opens a checkpoint and stores it in an inner array.
  param checkpoint_name: Helps identifying the opened checkpoint later
  """
  def open(self, checkpoint_name, checkpoint_path):
    checkpoint = pickle.load(open(checkpoint_path, "rb"))
    filters = pickle.loads(checkpoint["worker"])["filters"]
    for policy_key in filters.keys():
      assert isinstance(filters[policy_key], NoFilter), "Opening \
checkpoint with filters. That is currently not supported"
    self.paths[checkpoint_name] = checkpoint_path
    self.checkpoints[checkpoint_name] = checkpoint

  """
  Initializes a PPOTF policy and changes an already existing policy
  in the checkpoint with it.
  """
  def set_new_ppo_policy(self, checkpoint_name, policy_key):
    objs = pickle.loads(self.checkpoints[checkpoint_name]["worker"])
    assert objs["filters"][policy_key] is not None, "Trying to set PPO \
weights to a non-existing policy key inside checkpoint"
    if objs["state"][policy_key] is None:
      print("Setting new PPO policy weights to a previously 'None' \
policy. This might be correct if updating a dummy policy that wasn't \
originally returning any weights")
    objs["state"][policy_key] = self.__new_ppo_policy(policy_key)
    self.checkpoints[checkpoint_name]["worker"] = pickle.dumps(objs)
    pickle.dump(self.checkpoints[checkpoint_name], open(self.paths[checkpoint_name], "wb"))
    print("Successfully dumped modified checkpoint to " + self.paths[checkpoint_name])

  """
  Initializes a DQNTF policy and changes an already existing policy
  in the checkpoint with it.
  """
  def set_new_dqn_policy(self, checkpoint_name, policy_key):
    objs = pickle.loads(self.checkpoints[checkpoint_name]["worker"])
    assert objs["filters"][policy_key] is not None, "Trying to set DQN \
weights to a non-existing policy key inside checkpoint"
    if objs["state"][policy_key] is None:
      print("Setting new DQN policy weights to a previously 'None' \
policy. This might be correct if updating a dummy policy that wasn't \
originally returning any weights")
    objs["state"][policy_key] = self.__new_dqn_policy(policy_key)
    self.checkpoints[checkpoint_name]["worker"] = pickle.dumps(objs)
    pickle.dump(self.checkpoints[checkpoint_name], open(self.paths[checkpoint_name], "wb"))
    print("Successfully dumped modified checkpoint to " + self.paths[checkpoint_name])
    
  """
  Initializes a PGTF policy and changes an already existing policy
  in the checkpoint with it.
  """
  def set_new_pg_policy(self, checkpoint_name, policy_key):
    objs = pickle.loads(self.checkpoints[checkpoint_name]["worker"])
    assert objs["filters"][policy_key] is not None, "Trying to set PG \
weights to a non-existing policy key inside checkpoint"
    if objs["state"][policy_key] is None:
      print("Setting new PG policy weights to a previously 'None' \
policy. This might be correct if updating a dummy policy that wasn't \
originally returning any weights")
    objs["state"][policy_key] = self.__new_pg_policy(policy_key)
    print("hoooooooola")
    print(objs["state"]["zombie1_policy"])
    print(objs["state"]["citizen1_policy"])
    self.checkpoints[checkpoint_name]["worker"] = pickle.dumps(objs)
    pickle.dump(self.checkpoints[checkpoint_name], open(self.paths[checkpoint_name], "wb"))
    print("Successfully dumped modified checkpoint to " + self.paths[checkpoint_name])

  """
  Takes the policy keyed as 'citizen1_policy' and duplicates it n_times.
  Takes the policy keyed as 'zombie1_policy' and duplicates it m_times.

  Returns the path to the new checkpoint
  """
  def duplicate_policies(self, checkpoint_name, n_times, m_times):
    objs = pickle.loads(self.checkpoints[checkpoint_name]["worker"])
    c_weights = objs["state"]["citizen1_policy"]
    c_filter = objs["filters"]["citizen1_policy"]
    z_weights = objs["state"]["zombie1_policy"]
    z_filter = objs["filters"]["zombie1_policy"]
    assert (c_weights is not None) & (c_filter is not None), "Cannot replicate. Invalid checkpoint"
    assert (z_weights is not None) & (z_filter is not None), "Cannot replicate. Invalid checkpoint"
    for i in range(1, n_times + 2):
      key = "citizen" + str(i) + "_policy"
      objs["filters"][key] = z_filter
      objs["state"][key] = self.__changed_policy(c_weights, "citizen1_policy", key)
    for i in range(1, m_times + 2):
      key = "zombie" + str(i) + "_policy"
      objs["filters"][key] = z_filter
      objs["state"][key] = self.__changed_policy(z_weights, "zombie1_policy", key)
    self.checkpoints[checkpoint_name]["worker"] = pickle.dumps(objs)
    new_path = self.paths[checkpoint_name] + "_temp"
    print("filters")
    print(objs["filters"])
    pickle.dump(self.checkpoints[checkpoint_name], open(new_path, "wb"))
    return new_path
    
  """
  Initializes a PPOTF policy and changes an already existing policy
  in the checkpoint with it.
  """
  def update_weights(self, from_checkpoint_name, to_checkpoint_name, policy_key):
    objs1 = pickle.loads(self.checkpoints[from_checkpoint_name]["worker"])
    objs2 = pickle.loads(self.checkpoints[to_checkpoint_name]["worker"])
    assert objs2["filters"][policy_key] is not None, "Trying to update \
weights in a checkpoint that doesn't contain the specified policy " + policy_key
    objs2["state"][policy_key] = objs1["state"][policy_key]
    self.checkpoints[to_checkpoint_name]["worker"] = pickle.dumps(objs2)
    pickle.dump(self.checkpoints[to_checkpoint_name], open(self.paths[to_checkpoint_name], "wb"))
    print("Successfully updated " + policy_key + " from \
" + self.paths[from_checkpoint_name] + " to " + self.paths[to_checkpoint_name])

  # MARK:- Private

  def __new_ppo_policy(self, policy_key):
    new_policy_weights = PPOTFPolicy(self.obs_space, self.act_space, self.config).get_weights()
    return self.__update_prefix_if_needed(new_policy_weights, policy_key)

  def __new_dqn_policy(self, policy_key):
    new_policy_weights = DQNTFPolicy(self.obs_space, self.act_space, self.config).get_weights()
    return self.__update_prefix_if_needed(new_policy_weights, policy_key)
    
  def __new_pg_policy(self, policy_key):
    new_policy_weights = PGTFPolicy(self.obs_space, self.act_space, self.config).get_weights()
    return self.__update_prefix_if_needed(new_policy_weights, policy_key)

  def __update_prefix_if_needed(self, weights, new_prefix):
    # Raw initialized policy contains keys as fc_1/kernel', 'fc_1/bias',...
    # But in order to be correctly interpreted later by the trainer, they
    # should be prefixed like 'citizen1_policy/fc_1/kernel', etc.
    for key in weights.keys():
      if key.startswith(new_prefix) == False: # Avoid over-prefixing one checkpoint used many times
        weights[new_prefix + "/" + key] = weights.pop(key)
    return weights

  def __changed_policy(self, weights, old_prefix, new_prefix):
    updated_weights = deepcopy(weights)
    for key in weights.keys():
      assert key.startswith(old_prefix), "Wrong keyed weights"
      new_weight_key = new_prefix + key[key.find("/"):]
      updated_weights[new_weight_key] = updated_weights.pop(key)
    return updated_weights
