from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv

env = TownZombiesMultiagentWrapperEnv({"n_citizens": 1, "n_zombies": 1})
for i_episode in range(20000):
  observation = env.reset()
  for t in range(10000):
    env.render()
    actions = { "citizen1": env.action_space.sample(),
                "citizen2": env.action_space.sample(),
                "citizen3": env.action_space.sample(),
                "zombie1": env.action_space.sample(),
                "zombie2": env.action_space.sample() }
    observation, reward, done, info = env.step(actions)
    if done["__all__"] == True:
      print("Episode finished after {} timesteps".format(t+1))
      break
env.close()
