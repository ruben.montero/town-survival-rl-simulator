"""
Basic agent for town-evolution gym.
Keeps a table of state, action -> reward and searches for the optimum next action based on what it
has learnt.

See https://www.oreilly.com/learning/introduction-to-reinforcement-learning-and-openai-gym

Rubén Montero Vázquez. MUEI UDC 2019/20
"""

import gym
import gym_survival_multiagent
import numpy as np
import os
import errno
import time

"""
Map four x, y, bdg_x, bdg_y float coordinates to a state <= 9999

x:[1-9]y:[1-9]bdg_x:[1-9]bdg_y:[1-9]

e.g.: (x, y, bdg_x, bdg_y) = (0.2345, 0.1123, 0.6899, 0.7155) --> 2167

It's like "tiling" the environment to a 9x9 (total 81 tiles) canvas
"""
def q_state_with(gym_state):
  x = gym_state[0]
  y = gym_state[1]
  bdg_x = gym_state[6]
  bdg_y = gym_state[7]
  capped_x = max(0, min(int(x*10), 9))
  capped_y = max(0, min(int(y*10), 9))
  capped_bridge_x = max(0, min(int(bdg_x*10), 9))
  capped_bridge_y = max(0, min(int(bdg_y*10), 9))
  return int(str(capped_x) + str(capped_y)+str(capped_bridge_x) + str(capped_bridge_y))

def get_current_time_in_millis():
  return int(round(time.time() * 1000))

# Measurement of agent

EXPERIMENT_NUMBER = "003"

try:
    os.mkdir("./results/zombies-multiagent-dqn/%s"%EXPERIMENT_NUMBER)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
    pass

rewards_filename = "./results/zombies-multiagent-dqn/%s/mean_rewards_every_100_episodes_citizen.txt"%EXPERIMENT_NUMBER
rewards_filename_z1 = "./results/zombies-multiagent-dqn/%s/mean_rewards_every_100_episodes_zombie1.txt"%EXPERIMENT_NUMBER
rewards_filename_z2 = "./results/zombies-multiagent-dqn/%s/mean_rewards_every_100_episodes_zombie2.txt"%EXPERIMENT_NUMBER
rewards_filename_z3 = "./results/zombies-multiagent-dqn/%s/mean_rewards_every_100_episodes_zombie3.txt"%EXPERIMENT_NUMBER
bridge_interactions_filename = "./results/zombies-multiagent-dqn/%s/mean_bridge_interactions_every_100_episodes.txt"%EXPERIMENT_NUMBER
time_training_filename = "./results/zombies-multiagent-dqn/%s/time_training_in_millis_accum_every_100_episodes.txt"%EXPERIMENT_NUMBER

file_rewards = open(rewards_filename, 'w')
file_rewards_z1 = open(rewards_filename_z1, 'w')
file_rewards_z2 = open(rewards_filename_z2, 'w')
file_rewards_z3 = open(rewards_filename_z3, 'w')
file_bridge_interactions = open(bridge_interactions_filename, 'w')
file_time_training = open(time_training_filename, 'w')

# Create the gym
env = gym.make('town-survival-multiagent-v0')

# Credit: https://www.oreilly.com/learning/introduction-to-reinforcement-learning-and-openai-gym

Q_citizen = np.zeros([10000, env.action_space.n])
Q_zombie1 = np.zeros([10000, env.action_space.n])
Q_zombie2 = np.zeros([10000, env.action_space.n])
Q_zombie3 = np.zeros([10000, env.action_space.n])
alpha = 0.681

mean_episodes_to_dump_data = 100
sum_reward_citizen = 0
sum_reward_zombie1 = 0
sum_reward_zombie2 = 0
sum_reward_zombie3 = 0
sum_bridge_interactions = 0
start_time = get_current_time_in_millis()

for episode in range(0,100000):
  done = False
  citizen_state = env.reset()
  #Fixme: Right now, the first statuses for zombies are wrong
  zombie1_state = citizen_state
  zombie2_state = citizen_state
  zombie3_state = citizen_state
  G_citizen = 0
  G_zombie1 = 0
  G_zombie2 = 0
  G_zombie3 = 0

  while done != True:

    if ((episode % 1000) == 0):
      env.render()
      print('Rendering episode number {}...'.format(episode))

    # Citizen
    q_state_citizen = q_state_with(citizen_state)
    action = np.argmax(Q_citizen[q_state_citizen]) #1
    new_state, reward, done, info = env.step(action, 0) #2

    pesimistic_reward = reward - 1 # Ensure that the argmax always leads to a different action
                                   # by making the reward always negative, so that exploring a
                                   # new action is always better than taking an already taken
                                   # one, unless all 4 actions have already been explored.

    q_state_new_citizen = q_state_with(new_state)
    Q_citizen[q_state_citizen, action] += alpha * (pesimistic_reward + np.max(Q_citizen[q_state_new_citizen]) - Q_citizen[q_state_citizen, action])
    G_citizen += pesimistic_reward
    citizen_state = new_state

    # Zombie 1
    q_state_zombie1 = q_state_with(zombie1_state)
    action = np.argmax(Q_zombie1[q_state_zombie1])
    new_state, reward, done, info = env.step(action, 1)
    pesimistic_reward = reward - 1
    q_state_new_zombie1 = q_state_with(new_state)
    Q_zombie1[q_state_zombie1, action] += alpha * (pesimistic_reward + np.max(Q_zombie1[q_state_new_zombie1]) - Q_zombie1[q_state_zombie1, action])
    G_zombie1 += pesimistic_reward
    zombie1_state = new_state

    # Zombie 2
    q_state_zombie2 = q_state_with(zombie2_state)
    action = np.argmax(Q_zombie2[q_state_zombie2])
    new_state, reward, done, info = env.step(action, 2)
    pesimistic_reward = reward - 1
    q_state_new_zombie2 = q_state_with(new_state)
    Q_zombie2[q_state_zombie2, action] += alpha * (pesimistic_reward + np.max(Q_zombie2[q_state_new_zombie2]) - Q_zombie2[q_state_zombie2, action])
    G_zombie2 += pesimistic_reward
    zombie2_state = new_state

    # Zombie 3
    q_state_zombie3 = q_state_with(zombie3_state)
    action = np.argmax(Q_zombie3[q_state_zombie3])
    new_state, reward, done, info = env.step(action, 3)
    pesimistic_reward = reward - 1
    q_state_new_zombie3 = q_state_with(new_state)
    Q_zombie3[q_state_zombie3, action] += alpha * (pesimistic_reward + np.max(Q_zombie3[q_state_new_zombie3]) - Q_zombie3[q_state_zombie3, action])
    G_zombie3 += pesimistic_reward
    zombie3_state = new_state

    """
    First (#1): The agent starts by choosing an action with the highest Q value for the current
    state using argmax. Argmax will return the index/action with the highest value for that state.
    Initially, our Q table will be all zeros. But, after every step, the Q values for state-action
    pairs will be updated.

    Second (#2): The agent then takes action and we store the future state as state2 (St+1). This
    will allow the agent to compare the previous state to the new state.

    Third (#3): We update the state-action pair (St , At) for Q using the reward, and the max Q
    value for state2 (St+1). This update is done using the action value formula (based upon the
    Bellman equation) and allows state-action pairs to be updated in a recursive fashion (based
    on future values).
    """

  sum_reward_citizen += G_citizen
  sum_reward_zombie1 += G_zombie1
  sum_reward_zombie2 += G_zombie2
  sum_reward_zombie3 += G_zombie3
  bridge_interactions = info
  sum_bridge_interactions = sum_bridge_interactions + bridge_interactions

  if (episode != 0) & (episode % mean_episodes_to_dump_data == 0):
    print("Dumping debug data about training agent performance on files under results folder...")
    print('Episode {} finished. Mean reward in latest 100 episodes'.format(episode))
    print('Citizen = {}'.format(sum_reward_citizen / mean_episodes_to_dump_data))
    print('Zombie 1 = {}'.format(sum_reward_zombie1 / mean_episodes_to_dump_data))
    print('Zombie 2 = {}'.format(sum_reward_zombie2 / mean_episodes_to_dump_data))
    print('Zombie 3 = {}'.format(sum_reward_zombie3 / mean_episodes_to_dump_data))

    file_rewards.close()
    file_rewards_z1.close()
    file_rewards_z2.close()
    file_rewards_z3.close()
    file_bridge_interactions.close()
    file_time_training.close()

    file_rewards = open(rewards_filename, 'a')
    file_rewards_z1 = open(rewards_filename_z1, 'a')
    file_rewards_z2 = open(rewards_filename_z2, 'a')
    file_rewards_z3 = open(rewards_filename_z3, 'a')
    file_bridge_interactions = open(bridge_interactions_filename, 'a')
    file_time_training = open(time_training_filename, 'a')

    file_rewards.write('{},'.format(sum_reward_citizen / mean_episodes_to_dump_data))
    file_rewards_z1.write('{},'.format(sum_reward_zombie1 / mean_episodes_to_dump_data))
    file_rewards_z2.write('{},'.format(sum_reward_zombie2 / mean_episodes_to_dump_data))
    file_rewards_z3.write('{},'.format(sum_reward_zombie3 / mean_episodes_to_dump_data))
    file_bridge_interactions.write('{},'.format(sum_bridge_interactions / mean_episodes_to_dump_data))
    file_time_training.write('{},'.format(get_current_time_in_millis() - start_time))

    sum_reward_citizen = 0
    sum_reward_zombie1 = 0
    sum_reward_zombie2 = 0
    sum_reward_zombie3 = 0
    sum_bridge_interactions = 0

env.close()


