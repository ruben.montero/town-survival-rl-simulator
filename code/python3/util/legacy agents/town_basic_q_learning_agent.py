"""
Basic agent for town-evolution gym.
Keeps a table of state, action -> reward and searches for the optimum next action based on what it
has learnt.

See https://www.oreilly.com/learning/introduction-to-reinforcement-learning-and-openai-gym

Rubén Montero Vázquez. MUEI UDC 2019/20
"""

import gym
import gym_town_evolution
import numpy as np
import os
import time

"""
Map four x, y, bdg_x, bdg_y float coordinates to a state <= 9999

x:[1-9]y:[1-9]bdg_x:[1-9]bdg_y:[1-9]

e.g.: (x, y, bdg_x, bdg_y) = (0.2345, 0.1123, 0.6899, 0.7155) --> 2167

It's like "tiling" the environment to a 9x9 (total 81 tiles) canvas
"""
def q_state_with(gym_state):
  x, y, vel_x, vel_y, acc_x, acc_y, bdg_x, bdg_y = gym_state
  capped_x = max(0, min(int(x*10), 9))
  capped_y = max(0, min(int(y*10), 9))
  capped_bridge_x = max(0, min(int(bdg_x*10), 9))
  capped_bridge_y = max(0, min(int(bdg_y*10), 9))
  return int(str(capped_x) + str(capped_y)+str(capped_bridge_x) + str(capped_bridge_y))

def get_current_time_in_millis():
  return int(round(time.time() * 1000))

# Measurement of agent

EXPERIMENT_NUMBER = "005"

try:
    os.mkdir("./results/dqn-agent/%s"%EXPERIMENT_NUMBER)
except OSError as e:
        if e.errno != os.errno.EEXIST:
            raise   
        pass

rewards_filename = "./results/dqn-agent/%s/mean_rewards_every_100_episodes.txt"%EXPERIMENT_NUMBER
reached_goal_filename = "./results/dqn-agent/%s/mean_goals_reached_in_100_episodes_interval.txt"%EXPERIMENT_NUMBER
bridge_interactions_filename = "./results/dqn-agent/%s/mean_bridge_interactions_every_100_episodes.txt"%EXPERIMENT_NUMBER
time_training_filename = "./results/dqn-agent/%s/time_training_in_millis_accum_every_100_episodes.txt"%EXPERIMENT_NUMBER

file_rewards = open(rewards_filename, 'w')
file_reached_goal = open(reached_goal_filename, 'w')
file_bridge_interactions = open(bridge_interactions_filename, 'w')
file_time_training = open(time_training_filename, 'w')

# Create the gym
env = gym.make('town-evolution-forces-v0')

# Credit: https://www.oreilly.com/learning/introduction-to-reinforcement-learning-and-openai-gym

Q = np.zeros([10000, env.action_space.n])
alpha = 0.681

mean_episodes_to_dump_data = 100
sum_reward = 0
sum_bridge_interactions = 0
sum_goals_reached = 0
start_time = get_current_time_in_millis()

for episode in range(0,100000):
  done = False
  G, reward = 0,0
  state = env.reset()
  while done != True:

    if ((episode % 1000) == 0):
      env.render()
      print('Rendering episode number {}...'.format(episode))
      
    q_state = q_state_with(state)
    action = np.argmax(Q[q_state]) #1
    state2, reward, done, info = env.step(action) #2
    pesimistic_reward = reward - 1 # Ensure that the argmax always leads to a different action
                                   # by making the reward always negative, so that exploring a
                                   # new action is always better than taking an already taken
                                   # one, unless all 4 actions have already been explored.
    q_state2 = q_state_with(state2)
    Q[q_state, action] += alpha * (pesimistic_reward + np.max(Q[q_state2]) - Q[q_state, action]) #3
    G += pesimistic_reward
    state = state2

    """
    First (#1): The agent starts by choosing an action with the highest Q value for the current
    state using argmax. Argmax will return the index/action with the highest value for that state.
    Initially, our Q table will be all zeros. But, after every step, the Q values for state-action
    pairs will be updated.

    Second (#2): The agent then takes action and we store the future state as state2 (St+1). This
    will allow the agent to compare the previous state to the new state.

    Third (#3): We update the state-action pair (St , At) for Q using the reward, and the max Q
    value for state2 (St+1). This update is done using the action value formula (based upon the
    Bellman equation) and allows state-action pairs to be updated in a recursive fashion (based
    on future values).
    """

  sum_reward = sum_reward + G
  bridge_interactions, goals_reached = info
  sum_bridge_interactions = sum_bridge_interactions + bridge_interactions
  sum_goals_reached = sum_goals_reached + goals_reached
  
  if (episode != 0) & (episode % mean_episodes_to_dump_data == 0):
    print("Dumping debug data about training agent performance on files under results folder...")
    print('Episode {} finished. Mean in latest 100 episodes {}'.format(episode, sum_reward / mean_episodes_to_dump_data))

    file_rewards.close()
    file_reached_goal.close()
    file_bridge_interactions.close()
    file_time_training.close()

    file_rewards = open(rewards_filename, 'a')
    file_reached_goal = open(reached_goal_filename, 'a')
    file_bridge_interactions = open(bridge_interactions_filename, 'a')
    file_time_training = open(time_training_filename, 'a')

    file_rewards.write('{},'.format(sum_reward / mean_episodes_to_dump_data))
    file_bridge_interactions.write('{},'.format(sum_bridge_interactions / mean_episodes_to_dump_data))
    file_reached_goal.write('{},'.format(sum_goals_reached / mean_episodes_to_dump_data))
    file_time_training.write('{},'.format(get_current_time_in_millis() - start_time))

    sum_reward = 0
    sum_bridge_interactions = 0
    reached_goal_once = False

env.close()


