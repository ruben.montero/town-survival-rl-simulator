import gym
import gym_town_evolution

import gym
env = gym.make('town-evolution-forces-v0')
env.reset()
for i_episode in range(20000):
  observation = env.reset()
  for t in range(10000):
    env.render()
    action = env.action_space.sample()
    observation, reward, done, info = env.step(action)
    if done:
      print("Episode finished after {} timesteps".format(t+1))
      break
env.close()
