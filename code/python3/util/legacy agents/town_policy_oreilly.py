import tensorflow as tf
import numpy as np
import gym
import gym_town_evolution
import os

"""
Agent that tries to learn to solve the problem in town-evolution-forces-v0, at the point where:
- There's a bridge
- Agent has to interact with it to move it, so as to be able to cross the abyss
- Goal is at the other side

It will follow agent that plays doom as exposed here
https://www.oreilly.com/ideas/reinforcement-learning-with-tensorflow
...which in some way similar to the already implemented agent town_policy_gradient_agent.py, but
this time we'll remove a conv layer (because our agent doesn't need to view any "image", the
state of the gym is already passed as coordinates) and we'll fine tune it and measure it more
precisely, measuring the ANN performance in tensorboard.

Some other useful links:
https://adventuresinmachinelearning.com/neural-networks-tutorial/
https://adventuresinmachinelearning.com/python-tensorflow-tutorial/
https://adventuresinmachinelearning.com/stochastic-gradient-descent/
"""

env = gym.make('town-evolution-forces-v0')

# Environment Parameters
n_epochs = 10000
n = 0
average = []
step = 1
batch_size = 5000
render = True

# Define our three actions of moving forward, turning left & turning right
"""
choice = [[1,0,0,0,0,0,0,0,0],
          [0,1,0,0,0,0,0,0,0],
          [0,0,1,0,0,0,0,0,0],
          [0,0,0,1,0,0,0,0,0],
          [0,0,0,0,1,0,0,0,0],
          [0,0,0,0,0,1,0,0,0],
          [0,0,0,0,0,0,1,0,0],
          [0,0,0,0,0,0,0,1,0],
          [0,0,0,0,0,0,0,0,1]]
"""

states, actions, rewards = [], [], []

## Hyperparameters

alpha = 1e-4
gamma = 0.99
normalize_r = False
# save_path='models/town.ckpt'
value_scale = 0.5
entropy_scale = 0.00
gradient_clip = 40

"""
Simple way to evaluate all the rewards from an episode. To understand it better, take a glance
at the following example output:

> rewards = [0, 0, -75, 0, 0, 0, 100]
>
> print(discount(rewards, 0.1, False))
> [  0  -7 -74   0   1  10 100]
>
> print(discount(rewards, 0.95, False))
> [  5   6   6  85  90  95 100]
>
> print(discount(rewards, 0.95, True))
> [-1.164567 -1.141408 -1.141408  0.688153  0.80394  0.919743  1.0355386 ]
"""
def discount(r, gamma, normal):
    discount = np.zeros_like(r)
    G = 0.0
    for i in reversed(range(0, len(r))):
        G = G * gamma + r[i]
        discount[i] = G
    # Normalize 
    if normal:
        mean = np.mean(discount)
        std = np.std(discount)
        discount = (discount - mean) / (std)
    return discount

# Tensorflow Variables
X = tf.placeholder(tf.float32, (None,8), name='X')
Y = tf.placeholder(tf.int32, (None,), name='actions')
R = tf.placeholder(tf.float32, (None,), name='reward')
N = tf.placeholder(tf.float32, (None), name='episodes')
D_R = tf.placeholder(tf.float32, (None,), name='discounted_reward')

# Policy Network

dense = tf.layers.dense(
        inputs = X, 
        units = 32, 
        activation = tf.nn.elu,
        name = 'fc')

logits = tf.layers.dense(
         inputs = dense, 
         units = env.action_space.n, 
         name='logits')

value = tf.layers.dense(
        inputs=dense, 
        units = 1, 
        name='value')

calc_action = tf.multinomial(logits, 1)
aprob = tf.nn.softmax(logits)
action_logprob = tf.nn.log_softmax(logits)

tf.trainable_variables()

"""
Gather a bunch of training data from multiple episodes.
"""
def rollout(batch_size, render):

    states, actions, rewards, rewardsFeed, discountedRewards = [], [], [], [], []
    state = env.reset()
    episode_num = 0
    reward = 0

    while True: 

        if render:
            env.render()

        # Run State Through Policy & Calculate Action
        feed = {X: state.reshape(1, 8)}
        action = sess.run(calc_action, feed_dict=feed)
        action = action[0][0]
        state, reward, done, info = env.step(action)

        # Store Results
        states.append(state)
        rewards.append(reward)
        actions.append(action)

        if done:
            # Track Discounted Rewards
            rewardsFeed.append(rewards)
            discountedRewards.append(discount(rewards, gamma, normalize_r))

            if len(np.concatenate(rewardsFeed)) > batch_size:
                break

            # Save stats
            num_interactions_bridge, reached_goal = info
            episode_rewards_sum = np.sum(rewards)
            file_rewards.write('%r,'%episode_rewards_sum)
            file_bridge_interactions.write('%r,'%num_interactions_bridge)
            file_reached_goal.write('%s,'%reached_goal)

            # Reset Environment
            rewards = []
            state = env.reset()
            episode_num += 1

    return np.stack(states), np.stack(actions), np.concatenate(rewardsFeed), np.concatenate(discountedRewards), episode_num



mean_reward = tf.divide(tf.reduce_sum(R), N)

# Define Losses
pg_loss = tf.reduce_mean((D_R - value) * tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=Y))
value_loss = value_scale * tf.reduce_mean(tf.square(D_R - value))
entropy_loss = -entropy_scale * tf.reduce_sum(aprob * tf.exp(aprob))
loss = pg_loss + value_loss - entropy_loss

# Create Optimizer
optimizer = tf.train.AdamOptimizer(alpha)
grads = tf.gradients(loss, tf.trainable_variables())
grads, _ = tf.clip_by_global_norm(grads, gradient_clip) # gradient clipping
grads_and_vars = list(zip(grads, tf.trainable_variables()))
train_op = optimizer.apply_gradients(grads_and_vars)

# Initialize Session
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)


# Setup TensorBoard Writer
writer = tf.summary.FileWriter("/tmp/dpg")
tf.summary.scalar('Total_Loss', loss)
tf.summary.scalar('PG_Loss', pg_loss)
tf.summary.scalar('Entropy_Loss', entropy_loss)
tf.summary.scalar('Value_Loss', value_loss)
tf.summary.scalar('Reward_Mean', mean_reward)
tf.summary.histogram('FC layer', tf.trainable_variables()[0])
tf.summary.histogram('Logits', tf.trainable_variables()[2])
tf.summary.histogram('Value', tf.trainable_variables()[4])
write_op = tf.summary.merge_all()

"""
# Load model if exists
saver = tf.train.Saver(tf.global_variables())
load_was_success = True 
try:
    save_dir = '/'.join(save_path.split('/')[:-1])
    ckpt = tf.train.get_checkpoint_state(save_dir)
    load_path = ckpt.model_checkpoint_path
    saver.restore(sess, load_path)
except:
    print("No saved model to load. Starting new session")
    writer.add_graph(sess.graph)
    load_was_success = False
else:
    print("Loaded Model: {}".format(load_path))
    saver = tf.train.Saver(tf.global_variables())
    step = int(load_path.split('-')[-1])+1
"""

# Measurement of agent

EXPERIMENT_NUMBER = "011"

try:
    os.mkdir("./results/%s"%EXPERIMENT_NUMBER)
except OSError as e:
        if e.errno != os.errno.EEXIST:
            raise   
        pass

rewards_filename = "./results/%s/rewards_per_episode.txt"%EXPERIMENT_NUMBER
reached_goal_filename = "./results/%s/reached_goal_per_episode.txt"%EXPERIMENT_NUMBER
bridge_interactions_filename = "./results/%s/bridge_interactions_per_episode.txt"%EXPERIMENT_NUMBER

file_rewards = open(rewards_filename, 'w')
file_reached_goal = open(reached_goal_filename, 'w')
file_bridge_interactions = open(bridge_interactions_filename, 'w')

# Run

while step < n_epochs+1:
    # Gather Training Data
    print('Epoch', step)
    s, a, r, d_r, n = rollout(batch_size, render & ((n % 50) == 0))
    mean_reward = np.sum(r)/n
    average.append(mean_reward)
    print('Training Episodes: {}  Average Reward: {:4.2f}  Total Average: {:4.2f}'.format(n, mean_reward, np.mean(average)))

    # Update Network
    sess.run(train_op, feed_dict={X:s.reshape(len(s),8), Y:a, D_R: d_r})

    # Write TF Summaries
    summary = sess.run(write_op, feed_dict={X:s.reshape(len(s),8), Y:a, D_R: d_r, R: r, N:n})
    writer.add_summary(summary, step)
    writer.flush()

    # Save debug data
    #if step % 10 == 0:
    #      print("SAVED MODEL")
    #      saver.save(sess, save_path, global_step=step)
    if step % 20 == 0:
        print("Dumping debug data about training agent performance on files under results folder...")
        file_rewards.close()
        file_reached_goal.close()
        file_bridge_interactions.close()

        file_rewards = open(rewards_filename, 'a')
        file_reached_goal = open(reached_goal_filename, 'a')
        file_bridge_interactions = open(bridge_interactions_filename, 'a')

    step += 1
