"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20
"""

from interactive.zombies_lobby_manager import ZombiesLobbyManager
import argparse
import time

SERVER_NAME = "zombies"

parser = argparse.ArgumentParser(description="Launch lobbies for \
'Reinforcement Learning Zombies' client apps to register and participate \
in multiagent simulations. If you want to run this on a remote machine \
and leave it up after SSH logout, you might consider using \
'nohup your_command_script.sh &', as this program runs in a never ending \
loop (alternatively, it could be launched into a terminal multiplexer, \
such as screen or tmux)")
required_group = parser.add_argument_group('required arguments')
required_group.add_argument("--lobby", type=str, metavar="CHECKPOINT_PATH,N_CITIZENS,N_ZOMBIES", action="append", help="lobby to be held in this machine. You can pass this option multiple times to run several lobbies on the same port", required=True)
required_group.add_argument("--port", type=int, help="UDP port to listen to", required=True)

if __name__ == "__main__":
  args = parser.parse_args()
  manager = ZombiesLobbyManager(SERVER_NAME)
  manager.start_listening(args.port)
  for lobby in args.lobby:
    params = lobby.split(",", 3)
    checkpoint_path = params[0]
    n_citizens = int(params[1])
    n_zombies = int(params[2])
    manager.create_lobby(n_citizens, n_zombies, checkpoint_path)

  while True:
    time.sleep(1)
    manager.start_ready_lobbies()

