"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20

Subclass of RLLib MultiAgent environment that acts as a wrapper for
custom Gym environment gym_survival_multiagent.

@see
https://github.com/ray-project/ray/blob/master/rllib/env/multi_agent_env.py
https://gitlab.com/ruben.montero/gym-survival-multiagent/-/blob/master/gym_survival_multiagent/envs/town_survival_multiagent_env.py
"""

from ray.rllib.env.multi_agent_env import MultiAgentEnv
from gym_survival_multiagent.envs.town_survival_multiagent_env import TownZombiesMultiagentEnv
import gym
import gym_survival_multiagent

class TownZombiesMultiagentWrapperEnv(MultiAgentEnv):
  def __init__(self, config = {}):
    self.__env = gym.make('town-survival-multiagent-v1')
    self.__env.setup(config)
    self.action_space = self.__env.action_space
    self.observation_space = self.__env.observation_space
    self.n_citizens = self.__env.n_citizens
    self.n_zombies = self.__env.n_zombies
    self.state = {}
    for i in range(self.n_citizens + self.n_zombies):
      self.state[self.agent_key(i)] = None

  def reset(self):
    self.__env.reset()
    for i in range(self.n_citizens + self.n_zombies):
      name = self.agent_key(i)
      self.state[name] = self.__env.observation_for_agent(i)
    self.step_count = 0
    return self.state

  def step(self, action_dict):
    states = {}
    rewards = {}
    dones = {}
    infos = {}
    all_done = True
    for agent_i in range(self.n_citizens + self.n_zombies):
      name = self.agent_key(agent_i)
      action = action_dict[name]
      state, reward, done, info = self.__env.step(action, agent_i)
      states[name] = state
      rewards[name] = reward
      dones[name] = done
      infos[name] = info
      all_done = bool(all_done & done)
    dones["__all__"] = all_done
    self.state = states
    self.step_count += 1
    return states, rewards, dones, infos

  def agent_key(self, agent_index):
    if agent_index < self.n_citizens:
      return "citizen" + str(agent_index + 1)
    else:
      zombie_index = agent_index - self.n_citizens
      return "zombie" + str(zombie_index + 1)

  """
  Needed by interactive env, so that clients can render the actor.
  """
  def get_agent_absolute_positions(self):
    positions = []
    for state in self.__env.agents_states:
      positions.extend([state.ux, state.uy])
    return positions

  def close(self):
    self.__env.close()

  def render(self):
    self.__env.render()

  @staticmethod
  def observation_space():
    return TownZombiesMultiagentEnv.observation_space()

  @staticmethod
  def action_space():
    return TownZombiesMultiagentEnv.action_space()

  @staticmethod
  def default_config():
    return TownZombiesMultiagentEnv.default_config()
