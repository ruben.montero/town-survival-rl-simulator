"""
Rubén Montero Vázquez. MUEI UDC 2019/20

Trains RLLib agents for Gym Survival Multiagent.
@see
https://gitlab.com/ruben.montero/gym-survival-multiagent/-/blob/master/gym_survival_multiagent/envs/town_survival_multiagent_env.py
"""

from ray.rllib.utils import try_import_tf
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
from training.dqn_tf import DQNTF
from training.pg_tf import PGTF
from training.ppo_tf import PPOTF
from training.sac_tf import SACTF
import argparse
import sys
import ray

default = TownZombiesMultiagentWrapperEnv.default_config()
parser = argparse.ArgumentParser(description="Train an agent in order \
to be used in the hide 'n' seek style gym environment. Intended usage \
consists on first training a zombie and obtain checkpoint to be used \
for training a citizen afterwards. Training results can be viewed in \
Tensorboard if installed")

# Mandatory arguments
required_group = parser.add_argument_group('required arguments')
required_group.add_argument("--mode", type=str, help="'zombie' or 'citizen'", required=True) 
required_group.add_argument("--policy", type=str, help="type of algorithm to use. Supports 'dqn', 'ppo', 'pg'", required=True)
general_group = parser.add_argument_group('general options')
general_group.add_argument("--grid-search", help="if specified, other hyperparameters provided will be ignored and a default grid search configuration will be used", action='store_true')
general_group.add_argument("--iterations", type=int, default=1500)
general_group.add_argument("--checkpoint-dir", type=str, help="directory to periodically dump checkpoints for training")
general_group.add_argument("--zombie-checkpoint", type=str, help="path to a previously saved checkpoint for a pre-trained zombie in order to compete against a trainee citizen")
# Environment configuration arguments
env_group = parser.add_argument_group('environment options')
env_group.add_argument("--acceleration", type=int, default=default["acceleration"])
env_group.add_argument("--awareness-radius", type=int, default=default["awareness_radius"])
env_group.add_argument("--catch-distance", type=int, default=default["catch_distance"])
env_group.add_argument("--citizen-max-speed", type=int, default=default["citizen_max_speed"])
env_group.add_argument("--episode-steps", type=int, default=default["episode_steps"])
env_group.add_argument("--prep-phase-steps", type=int, default=default["prep_phase_steps"])
env_group.add_argument("--zombie-max-speed", type=int, default=default["zombie_max_speed"])
# Training hyperparameters arguments
# DQN
hyp_group = parser.add_argument_group('training hyperparameters')
hyp_group.add_argument("--dueling", action='store_true')
hyp_group.add_argument("--gamma", type=float, default=0.95)
hyp_group.add_argument("--prioritized-replay", action='store_true')
# PG + PPO
hyp_group.add_argument("--batch-size", type=int, default=8000)
hyp_group.add_argument("--learning-rate", type=float, default=0.0005)
# PPO
hyp_group.add_argument("--clip-param", type=float, default=0.1)
# SAC ## sac is still experimental
ppo_group = parser.add_argument_group('sac training hyperparameters')
hyp_group.add_argument("--tau", type=int, default=0.005)
hyp_group.add_argument("--actor-lr", type=float, default=0.0003)
hyp_group.add_argument("--critic-lr", type=float, default=0.0003)


if __name__ == "__main__":
  args = parser.parse_args()
  if args.mode not in ["zombie", "citizen"]:
    print("No valid mode. You must either train a citizen or a zombie. Use --help")
    sys.exit()
  if args.policy not in ["ppo", "dqn", "sac", "pg"]:
    print("No valid policy. Use --help")
    sys.exit()

  tf = try_import_tf()
  ray.init()

  env_config = {
    "episode_steps": args.episode_steps,
    "prep_phase_steps": args.prep_phase_steps,
    "acceleration": args.acceleration,
    "catch_distance": args.catch_distance,
    "awareness_radius": args.awareness_radius,
    "citizen_max_speed": args.citizen_max_speed,
    "zombie_max_speed": args.zombie_max_speed,
    "n_citizens": 1,
    "n_zombies": 1
  }

  if args.policy == "ppo":
    if args.grid_search == True:
      PPOTF().run_grid_search(args.mode,
                              env_config,
                              args.iterations,
                              args.checkpoint_dir,
                              args.zombie_checkpoint)
    else:
      PPOTF().run_training(args.mode,
                           env_config,
                           args.iterations,
                           args.batch_size,
                           args.clip_param,
                           args.learning_rate,
                           args.checkpoint_dir,
                           args.zombie_checkpoint)
  elif args.policy == "dqn":
    if args.grid_search == True:
      DQNTF().run_grid_search(args.mode,
                              env_config,
                              args.iterations,
                              args.checkpoint_dir,
                              args.zombie_checkpoint)
    else:
      DQNTF().run_training(args.mode,
                           env_config,
                           args.iterations,
                           args.prioritized_replay,
                           args.dueling,
                           args.gamma,
                           args.checkpoint_dir,
                           args.zombie_checkpoint)
  elif args.policy == "sac":
    if args.grid_search == True:
      SACTF().run_grid_search(args.mode,
                              env_config,
                              args.iterations,
                              args.checkpoint_dir,
                              args.zombie_checkpoint)
    else:
      SACTF().run_training(args.mode,
                           env_config,
                           args.iterations,
                           args.tau,
                           args.actor_lr,
                           args.critic_lr,
                           args.checkpoint_dir,
                           args.zombie_checkpoint)
  elif args.policy == "pg":
    if args.grid_search == True:
      PGTF().run_grid_search(args.mode,
                             env_config,
                             args.iterations,
                             args.checkpoint_dir,
                             args.zombie_checkpoint)
    else:
      PGTF().run_training(args.mode,
                           env_config,
                           args.iterations,
                           args.learning_rate,
                           args.batch_size,
                           args.checkpoint_dir,
                           args.zombie_checkpoint)
  else:
    assert False, "No supported policy"
