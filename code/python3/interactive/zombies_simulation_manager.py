"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20
"""

from threading import Thread
from interactive.zombies_simulation import ZombiesSimulation
import socket

RECV_BUFFER_SIZE  = 1024

def listen_loop(simulation_manager):
  print("Started listening on " + str(simulation_manager.socket.getsockname()) + "...")
  try:
    while True:
      d = simulation_manager.socket.recvfrom(RECV_BUFFER_SIZE)
      data = d[0]
      addr = d[1]
      if data is not None:
        datagram = data.decode('utf-8')
        if datagram.startswith(simulation_manager.server_name):
          trim_prefix = datagram[len(simulation_manager.server_name)+1:] # Remove 'zombies:'
          separator_pos = trim_prefix.find(";")
          if separator_pos > 0:
            try:
              simulation_number = int(trim_prefix[:separator_pos])
              simulation = simulation_manager.simulations.get(simulation_number, None)
              if simulation is not None:
                handled = False
                for i in range(len(simulation.clients)):
                  if addr == simulation.clients[i]:
                    content = trim_prefix[separator_pos+1:]
                    if content.startswith("action="):
                      value = content[len("action="):]
                      try:
                        recv_action = int(value)
                        if recv_action >= 0 & recv_action < 5:
                          print("Received valid action " + str(recv_action))
                          simulation.last_received_action[i] = recv_action
                          simulation.inactive_steps = 0
                          handled = True
                        else:
                          print("Received invalid action from " + str(addr))
                      except ValueError:
                        print("Received invalid action from " + str(addr))
                    else:
                      print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
                if handled:
                  print("Properly handled action from" + str(addr))
                else:
                  print("Received message from unregistered client " + str(addr))
              else:
                print(simulation_manager.simulations)
                print("Received UDP packet from " + str(addr) + " for inexistent simulation number " + str(simulation_number))
            except ValueError:
              print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
          else:
            print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
        else:
          print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
      else:
        print("Received empty packet from " + str(addr))
  except:
    print("Finished listening")

class ZombiesSimulationManager():
  def __init__(self, server_name):
    self.simulations = {}
    self.server_name = server_name

  def start_listening(self, port):
    self.port = port
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.socket.bind(("localhost", self.port))
    self.listening_thread = Thread(target = listen_loop, args = (self,))
    self.listening_thread.daemon = True
    self.listening_thread.start()

  def create_and_run(self, from_lobby, number):
    agents = []
    for agent in from_lobby.registered_agents:
      agents.append(agent[0])
    self.simulations[number] = ZombiesSimulation(agents,
                                                 self.socket,
                                                 from_lobby.n_citizens,
                                                 from_lobby.n_zombies,
                                                 from_lobby.checkpoint_path,
                                                 self.server_name,
                                                 number)
    self.simulations[number].start()
