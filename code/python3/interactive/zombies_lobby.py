"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20
"""

import socket
import time

class ZombiesLobby():
  def __init__(self,
               number,
               server_name,
               simulation_port,
               n_citizens,
               n_zombies,
               checkpoint_path):
    assert (n_citizens > 0) & (n_zombies > 0), "Invalid number of agents"
    self.number = number
    self.server_name = server_name
    self.simulation_port = simulation_port
    self.n_citizens = n_citizens
    self.n_zombies = n_zombies
    self.checkpoint_path = checkpoint_path
    self._is_countdown_started = False
    self.registered_agents = []
    for _ in range(n_citizens + n_zombies):
      self.registered_agents.append(self.__default_agent())

  def should_begin(self):
    all_agents_are_cpus = True
    for agent in self.registered_agents:
      if agent[0] is not None:
        all_agents_are_cpus = False
        if agent[2] == False: # Client not ready
          return False
    return not all_agents_are_cpus

  """
  Will perform countdown sending message to clients to render.
  e.g.: zombies:0;message=Message to be rendered by client

  Last message will tell the clients the UDP port for the simulation.
  e.g.: zombies:0;start=port:12345
  """
  def blocking_countdown(self, socket):
    self._is_countdown_started = True
    for i in range(3, 0, -1):
      msg = self.__header() + ";message=All human agents are ready. Starting in " + str(i)
      self._send_message_to_all(msg, socket)
      time.sleep(1)
    self._send_message_to_all(self.__header() + ";start=port:" + str(self.simulation_port), socket)

  # MARK:- Soft private (to be used by server thread)

  def _send_status_to_all(self, socket):
    self._send_message_to_all(self._lobby_status_response(), socket)

  def _send_message_to_all(self, raw_string, socket):
    for agent in self.registered_agents:
      if agent[0] is not None:
        socket.sendto(raw_string.encode(), agent[0])

  """
  Returns the body for the UDP packet to inform clients of the lobby
  status.
  e.g.: zombies:0;agent1=ai,citizen,Random,ready;agent2=human,zombie,Pepe,not_ready
  """
  def _lobby_status_response(self):
    response = self.__header() + ";"
    for i in range(len(self.registered_agents)):
      agent = self.registered_agents[i]
      response += "agent" + str(i) + "="
      response += "open" if agent[0] is None else "close"
      response += ","
      response += "citizen" if i < self.n_citizens else "zombie"
      response += ","
      response += str(agent[1])
      response += ","
      response += "ready" if agent[2] == True else "not_ready"
      if i < len(self.registered_agents) - 1:
        response += ";"
    return response

  """
  Handles a registration request from a client.

  Example of registration body request:
  zombie:0;register=agent3,ClientSpecifiedTag
  - param registration_value: String part of registration body request
                              from '(...)register=' prefix.

  Note that a client is allowed to register multiple times, but every
  time it requests a new registration, it's removed from its previous
  slot.
  """
  def _registration_attempt(self, registration_value, addr, socket):
    if registration_value.startswith("agent"):
      removed_prefix = registration_value[len("agent"):]
      # Nice-to-have: This is probably easily broken by a malicious client
      agent_index = int(removed_prefix[:removed_prefix.find(",")])
      assert agent_index < len(self.registered_agents), "Received invalid index"
      if self.registered_agents[agent_index][0] is None:
        self.__ensure_unregistered(addr)
        # Nice-to-have: This is probably easily broken by a malicious client
        client_tag = removed_prefix[removed_prefix.find(",")+1:].rsplit()[0]
        self.registered_agents[agent_index] = (addr, client_tag, False)
        socket.sendto(self.__registered_response(agent_index).encode(), addr)
        return True
    return False

  """
  Handles a set_ready request from a client.

  Example of set_ready body request:
  zombie:0;ready=agent3,true
  - param registration_value: String part of set_ready body request
                              from '(...)ready=' prefix.
  """
  def _set_ready_attempt(self, set_ready_value, addr):
    if set_ready_value.startswith("agent"):
      removed_prefix = set_ready_value[len("agent"):]
      # Nice-to-have: This is probably easily broken by a malicious client
      agent_index = int(removed_prefix[:removed_prefix.find(",")])
      assert agent_index < len(self.registered_agents), "Received invalid index"
      agent = self.registered_agents[agent_index]
      value = removed_prefix[removed_prefix.find(",")+1:]
      if value == "true":
        self.registered_agents[agent_index] = (agent[0], agent[1], True)
        return True
      elif value == "false":
        self.registered_agents[agent_index] = (agent[0], agent[1], False)
        return True
    return False

  # MARK:- Private

  def __default_agent(self):
     return (None, "Random", True) # IP address, name, isReady

  def __registered_response(self, agent_index):
    return self.__header() + ";registered=agent" + str(agent_index)

  def __ensure_unregistered(self, addr):
    for i in range(len(self.registered_agents)):
      if self.registered_agents[i][0] == addr:
        self.registered_agents[i] = self.__default_agent()

  def __header(self):
    return self.server_name + ":" + str(self.number)
