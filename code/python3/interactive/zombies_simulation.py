"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20
"""

from interactive.town_zombies_interactive_env import TownZombiesInteractiveEnv
from ray.rllib.agents.ppo.ppo_tf_policy import PPOTFPolicy
from ray.rllib.evaluation.rollout_worker import RolloutWorker
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
from training.no_move_policy import NoMovePolicy
from util.checkpoint_adapter import CheckpointAdapter
import pickle
import socket
import time

DEFAULT_ACTION       = 0
CLOSE_INACTIVE_STEPS = 300

class ZombiesSimulation():
  """
  Receives an array of inet_addresses.
  <None> values are interpreted as CPU.
  """
  def __init__(self,
               client_addresses,
               socket,
               n_citizens,
               n_zombies,
               checkpoint_path,
               server_name,
               number):
    assert n_citizens + n_zombies == len(client_addresses), "Wrong parameters"
    self.last_received_action = [DEFAULT_ACTION] * len(client_addresses)
    self.clients = client_addresses
    self.socket = socket
    self.server_name = server_name
    self.number = number
    obs_space = TownZombiesMultiagentWrapperEnv.observation_space()
    act_space = TownZombiesMultiagentWrapperEnv.action_space()
    policies = {}
    for i in range(n_citizens + n_zombies):
      policy_type = PPOTFPolicy if client_addresses[i] is None else NoMovePolicy
      if i < n_citizens:
        policies["citizen" + str(i+1) + "_policy"] = (policy_type, obs_space, act_space, {})
      else:
        policies["zombie" + str(i+1-n_zombies) + "_policy"] = (policy_type, obs_space, act_space, {})
    #print(policies)
    #extra_data = pickle.load(open(checkpoint_path, "rb"))
    self.rollout_worker = RolloutWorker(
      env_creator = lambda _: TownZombiesInteractiveEnv({
                                                          "n_citizens": n_citizens,
                                                          "n_zombies": n_zombies,
                                                          #"citizen_max_speed": 0.01,
                                                          #"zombie_max_speed": 0.005
                                                        }, self),
      policy=policies,
      policy_mapping_fn=lambda c: c + "_policy",
      rollout_fragment_length=TownZombiesMultiagentWrapperEnv.default_config()["episode_steps"]
    )
    adapter = CheckpointAdapter(obs_space, act_space)
    adapter.open("temp", checkpoint_path)
    new_path = adapter.duplicate_policies("temp", n_citizens - 1, n_zombies - 1)
    extra_data = pickle.load(open(new_path, "rb"))
    self.rollout_worker.restore(extra_data["worker"])
    self.inactive_steps = 0
    self.should_run = True

  def start(self):
    while self.should_run == True:
      self.rollout_worker.sample()
      
  """
  Sends observation to registered clients over UDP.
  e.g.: zombies:0:1590437517102:191;obs=0.1379,0.7637,0.0964,0.0176,0.4964,-0.489,1.0,1.0,1.0,1.0;reward=-1;done=false
  """
  def send_over_udp(self,
                    observations,
                    rewards,
                    dones,
                    step_count,
                    agent_key_mapping_fn,
                    agent_abs_pos_fn):
    round_accuracy = 4
    ts_millis = round(time.time()*1000)
    for i in range(len(self.clients)):
      if self.clients[i] is not None:
        datagram = self.server_name + ":" + str(self.number) + ":" + str(ts_millis) + ":" + str(step_count) + ";obs="
        agent_obs = observations[agent_key_mapping_fn(i)]
        for j in range(len(agent_obs)):
          datagram += str(round(agent_obs[j], round_accuracy))
          if j < (len(agent_obs) - 1):
            datagram += ","
        datagram += ";reward=" + str(round(rewards[agent_key_mapping_fn(i)], round_accuracy))
        datagram += ";done=" + ("true" if dones[agent_key_mapping_fn(i)] == True else "false")
        all_coordinates = agent_abs_pos_fn(i)
        datagram += ";extra="
        for (k, coordinate) in enumerate(all_coordinates):
          datagram += str(round(coordinate, round_accuracy))
          if k < (len(all_coordinates) - 1):
            datagram += ","
        print("Sending datagram " + datagram + " to " + str(self.clients[i]))
        self.socket.sendto(datagram.encode(), self.clients[i])

  def increase_inactive_steps(self):
    self.inactive_steps += 1
    if self.inactive_steps > CLOSE_INACTIVE_STEPS:
      self.should_run = False
