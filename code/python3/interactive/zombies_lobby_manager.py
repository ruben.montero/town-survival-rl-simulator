"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20
"""

from interactive.zombies_lobby import ZombiesLobby
from interactive.zombies_simulation_manager import ZombiesSimulationManager
from threading import Thread
import socket
import time

RECV_BUFFER_SIZE     = 1024

def listen_loop(lobby_manager):
  print("Initializing lobby server on " + str(lobby_manager.socket.getsockname()) + "...")
  try:
    while True:
      d = lobby_manager.socket.recvfrom(RECV_BUFFER_SIZE)
      data = d[0]
      addr = d[1]
      if data is not None:
        datagram = data.decode('utf-8')
        if datagram.startswith(lobby_manager.server_name):
          trim_prefix = datagram[len(lobby_manager.server_name)+1:] # Remove 'zombies:'
          separator_pos = trim_prefix.find(";")
          if separator_pos > 0:
            try:
              lobby_number = int(trim_prefix[:separator_pos])
              if lobby_number < len(lobby_manager.lobbies):
                lobby = lobby_manager.lobbies[lobby_number]
                if lobby._is_countdown_started == True:
                  print("Received packet from " + str(addr) + " but countdown has started for lobby " + str(lobby_number))
                else:
                  content = trim_prefix[separator_pos+1:]
                  if content == "lobby":
                    print("Received lobby status request. Replying...")
                    lobby_manager.socket.sendto(lobby._lobby_status_response().encode(), addr)
                  elif content.startswith("register="):
                    value = content[len("register="):]
                    if lobby._registration_attempt(value, addr, lobby_manager.socket):
                      print("Successfully completed registration with value " + value)
                      lobby._send_status_to_all(lobby_manager.socket)
                    else:
                      print("Registration unsuccessful for value " + value)
                  elif content.startswith("ready="):
                    value = content[len("ready="):]
                    if lobby._set_ready_attempt(value, addr):
                      print("Successfully completed ready request with value " + value)
                      lobby._send_status_to_all(lobby_manager.socket)
                    else:
                      print("Ready request unsuccessful for value " + value)
                  else:
                    print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
              else:
                print("Received UDP packet from " + str(addr) + " for inexistent lobby number " + str(lobby_number)) 
            except ValueError:
              print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
          else:
            print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
        else:
          print("Received malformed UDP packet from " + str(addr) + ". Content = " + datagram)
      else:
        print("Received empty packet from " + str(addr))
  except _:
    print("Finished listening")

class ZombiesLobbyManager():
  def __init__(self, server_name):
    self.lobbies = []
    self.server_name = server_name
    self.socket = None
    # Maybe it's nice to decouple ZombiesSimulationManager from ZombiesLobbyManager
    self.simulation_manager = ZombiesSimulationManager(server_name)

  def start_listening(self, port):
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.socket.bind(("localhost", port))
    self.listening_thread = Thread(target = listen_loop, args = (self,))
    self.listening_thread.daemon = True
    self.listening_thread.start()
    self.simulation_manager.start_listening(port+1)

  def create_lobby(self, n_citizens, n_zombies, checkpoint_path):
    lobby = ZombiesLobby(len(self.lobbies),
                         self.server_name,
                         self.simulation_manager.port,
                         n_citizens,
                         n_zombies,
                         checkpoint_path)
    self.lobbies.append(lobby)

  def start_ready_lobbies(self):
    for (i, lobby) in enumerate(self.lobbies):
      if (lobby._is_countdown_started == False) & (lobby.should_begin() == True):
        lobby.blocking_countdown(self.socket)
        self.simulation_manager.create_and_run(lobby, i)
