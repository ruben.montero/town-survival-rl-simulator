"""
Rubén Montero Vázquez.
Master's Degree Project. University of A Coruña 2019/20

Subclass of TownZombiesMultiagentWrapperEnv that supports networking
for integration with Godot.
"""

from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv

import time

MAX_FPS = 15

class TownZombiesInteractiveEnv(TownZombiesMultiagentWrapperEnv):
  def __init__(self, config, delegate):
    self.delegate = delegate
    self.__step_duration = 1/MAX_FPS
    super(TownZombiesInteractiveEnv, self).__init__(config)

  def step(self, action_dict):
    # Override actions with UDP-received actions
    for i in range(len(self.delegate.clients)):
      if self.delegate.clients[i] is not None:
        action_dict[self.agent_key(i)] = self.delegate.last_received_action[i]
    # Measure step time and wait if needed
    before = time.time()
    obs, rewards, done, info = super(TownZombiesInteractiveEnv, self).step(action_dict)
    self.delegate.send_over_udp(obs,
                                rewards,
                                done,
                                self.step_count,
                                lambda i: self.agent_key(i),
                                lambda i: self.get_agent_absolute_positions())
    after = time.time()
    if after - before < self.__step_duration:
      time.sleep(self.__step_duration - (after - before))
    self.delegate.increase_inactive_steps()
    return obs, rewards, done, info
