"""
Rubén Montero Vázquez. MUEI UDC 2019/20
"""

from ray.rllib.agents.sac.sac import SACTrainer
from ray.rllib.agents.sac.sac_tf_policy import SACTFPolicy
from ray.tune.logger import pretty_print
from ray.tune.registry import register_env
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
from training.no_move_policy import NoMovePolicy
from training.town_callbacks import TownZombiesCallbacks
from util.checkpoint_adapter import CheckpointAdapter
import os
import pickle

ENV_REGISTRATION_NAME = "TownSurvivalSACPolicy"
CHECKPOINT_FREQ = 50
CHECKPOINT_ROOT_DIR = "checkpoint_sac"
GRID_SEARCH_TAUS = [0.0001, 0.005, 0.001]
GRID_SEARCH_ACTOR_LR = [0.0003, 0.003]
GRID_SEARCH_CRITIC_LR = [0.0003, 0.003]

class SACTF():

  def run_grid_search(self,
                      mode,
                      env_config,
                      n_iters,
                      checkpoint_base_dir,
                      zombie_checkpoint):
    for tau in GRID_SEARCH_TAUS:
      for actor_lr in GRID_SEARCH_ACTOR_LR:
        for critic_lr in GRID_SEARCH_CRITIC_LR:
          self.run_training(mode,
                            env_config,
                            n_iters,
                            tau,
                            actor_lr,
                            critic_lr,
                            checkpoint_base_dir,
                            zombie_checkpoint)

  def run_training(self,
                   mode,
                   env_config,
                   n_iters,
                   tau,
                   actor_lr,
                   critic_lr,
                   checkpoint_base_dir,
                   zombie_checkpoint):
    if checkpoint_base_dir is not None:
      checkpoint_dir = checkpoint_base_dir + "/" + CHECKPOINT_ROOT_DIR 
      checkpoint_dir += "/tau" + str(tau) + "_alr" + str(actor_lr) + "_clr" + str(critic_lr)
      os.makedirs(checkpoint_dir)
    if mode == "citizen":
      assert zombie_checkpoint is not None, "Missing zombie checkpoint.\
Training a citizen, so a checkpoint for the zombie must be provided with --zombie-checkpoint"
    env_registration_name = ENV_REGISTRATION_NAME 
    env_registration_name += ("Citizen" if mode == "citizen" else "Zombie")
    env_registration_name += "_tau" + str(tau) 
    env_registration_name += "_alr" + str(actor_lr) + "_clr" + str(critic_lr)
    register_env(env_registration_name, lambda _: TownZombiesMultiagentWrapperEnv(env_config))

    trainer = self.__create_trainer(mode,
                                    env_config,
                                    env_registration_name,
                                    tau,
                                    actor_lr,
                                    critic_lr,
                                    zombie_checkpoint)

    # Run training iterations
    for i in range(n_iters):
      print("== Iteration", i, "==")
      print(pretty_print(trainer.train()))
      # Checkpointing
      if checkpoint_base_dir is not None:
        if (i % CHECKPOINT_FREQ) == 0:
          print("Creating folder for checkpoint in iteration ", i)
          folder_name = checkpoint_dir + "/" + str(i)
          os.mkdir(folder_name)
          trainer.save(folder_name)

  # MARK:- Private

  def __create_trainer(self,
                       mode,
                       env_config,
                       env_registration_name,
                       tau,
                       actor_lr,
                       critic_lr,
                       zombie_checkpoint):
    obs_space = TownZombiesMultiagentWrapperEnv.observation_space()
    act_space = TownZombiesMultiagentWrapperEnv.action_space()
    policies = {
      "citizen1_policy": (SACTFPolicy if mode == "citizen" else NoMovePolicy, obs_space, act_space, {}),
      "zombie1_policy": (SACTFPolicy, obs_space, act_space, {})
    }
    trainer = SACTrainer(
      env=env_registration_name,
      config={
        "multiagent": {
          "policies": policies,
          "policy_mapping_fn": lambda c: c + "_policy",
          "policies_to_train": ["citizen1_policy" if mode == "citizen" else "zombie1_policy"],
        },
        "callbacks": TownZombiesCallbacks,
        "normalize_actions": False,
        "tau": tau,
        "optimization": {
          "actor_learning_rate": actor_lr,
          "critic_learning_rate": critic_lr,
        }
      })
    if mode == "citizen":
      self.__restore_checkpoint(trainer, zombie_checkpoint, obs_space, act_space)
    return trainer

  def __restore_checkpoint(self, trainer, zombie_checkpoint, obs_space, act_space):
    try:
      trainer.restore(zombie_checkpoint)
    except:
      print("An exception occured while attempting to restore zombie_checkpoint. \
This happens when using a freshly created checkpoint with --mode zombie, because \
zombies train against a citizen 'NoMovePolicy' and that can't be restored in a \
SACTrainer prepared for training a citizen 'SACTFPolicy'. Will try to adapt it...")
      adapter = CheckpointAdapter(obs_space, act_space)
      adapter.open("to_update", zombie_checkpoint)
      assert False, "uninmplemented"
      adapter.set_new_ppo_policy("to_update", "citizen1_policy")
      trainer.restore(zombie_checkpoint)
      print("Successfully restored checkpoint with trained zombie at " + zombie_checkpoint)
