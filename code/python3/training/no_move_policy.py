"""
Rubén Montero Vázquez. MUEI UDC 2019/20
"""

from ray.rllib.policy.policy import Policy

class NoMovePolicy(Policy):
  def compute_actions(self,
                      obs_batch,
                      state_batches=None,
                      prev_action_batch=None,
                      prev_reward_batch=None,
                      info_batch=None,
                      episodes=None,
                      **kwargs):
    return [0 for _ in obs_batch], [], {}

  def get_weights(self):
    return None

  def set_weights(self, weights):
    pass

  def learn_on_batch(self, samples):
    return {}
