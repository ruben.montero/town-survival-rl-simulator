"""
Rubén Montero Vázquez. MUEI UDC 2019/20
"""

from ray.rllib.agents.callbacks import DefaultCallbacks
from ray.rllib.env import BaseEnv
from ray.rllib.evaluation import MultiAgentEpisode, RolloutWorker
from ray.rllib.policy import Policy
from ray.rllib.policy.sample_batch import SampleBatch
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
from typing import Dict
import numpy as np

class TownZombiesCallbacks(DefaultCallbacks):
  """
  Example from
  https://github.com/ray-project/ray/blob/0c80efa2a37f482494fbffbe9e81f61586b03ecb/rllib/examples/custom_metrics_and_callbacks.py
  """
  def on_episode_start(self, worker: RolloutWorker, base_env: BaseEnv,
                       policies: Dict[str, Policy],
                       episode: MultiAgentEpisode, **kwargs):
    episode.user_data["episode_step"] = 0
    episode.user_data["n_citizens"] = worker.env.n_citizens
    episode.user_data["n_zombies"] = worker.env.n_citizens
    for i in range(episode.user_data["n_citizens"]):
      episode.user_data["citizen" + str(i+1) + "_explored_tiles"] = 0
      episode.user_data["citizen" + str(i+1) + "_steps_in_good_hiding_place"] = 0
      for j in range(episode.user_data["n_zombies"]):
        episode.user_data["citizen" + str(i+1) + "_distance_to_zombie" + str(j+1)] = []
        episode.hist_data["citizen" + str(i+1) + "_distance_to_zombie" + str(j+1)] = []
    for i in range(episode.user_data["n_zombies"]):
      episode.user_data["zombie" + str(i+1) + "_explored_tiles"] = 0
      for j in range(episode.user_data["n_citizens"]):
        episode.user_data["zombie" + str(i+1) + "_distance_to_citizen" + str(j+1)] = []
        episode.hist_data["zombie" + str(i+1) + "_distance_to_citizen" + str(j+1)] = []

  def on_episode_step(self, worker: RolloutWorker, base_env: BaseEnv,
                      episode: MultiAgentEpisode, **kwargs):
    episode.user_data["episode_step"] += 1
    for i in range(episode.user_data["n_citizens"]):
      agent = "citizen" + str(i+1)
      episode.user_data[agent + "_explored_tiles"] += episode.last_info_for(agent).get("is_visiting_new_tile", 0)
      episode.user_data[agent + "_steps_in_good_hiding_place"] += episode.last_info_for(agent).get("is_in_good_hiding_place", 0)
      for j in range(episode.user_data["n_zombies"]):
        zombie = "zombie" + str(j+1)
        episode.user_data[agent + "_distance_to_" + zombie].append(episode.last_info_for(agent).get("distance_to_" + zombie, 0))
    for i in range(episode.user_data["n_zombies"]):
      agent = "zombie" + str(i+1)
      episode.user_data["zombie" + str(i+1) + "_explored_tiles"] += episode.last_info_for(agent).get("is_visiting_new_tile", 0)
      for j in range(episode.user_data["n_citizens"]):
        citizen = "citizen" + str(j+1)
        episode.user_data[agent + "_distance_to_" + citizen].append(episode.last_info_for(agent).get("distance_to_" + citizen, 0))

  def on_episode_end(self, worker: RolloutWorker, base_env: BaseEnv,
                      policies: Dict[str, Policy], episode: MultiAgentEpisode,
                      **kwargs):
    for i in range(episode.user_data["n_citizens"]):
      agent = "citizen" + str(i+1)
      episode.custom_metrics[agent + "_explored_tiles"] = episode.user_data[agent + "_explored_tiles"]
      episode.custom_metrics[agent + "_steps_in_good_hiding_place"] = episode.user_data[agent + "_steps_in_good_hiding_place"]
      for j in range(episode.user_data["n_zombies"]):
        zombie = "zombie" + str(j+1)
        episode.custom_metrics[agent + "_distance_to_" + zombie] = np.mean(episode.user_data[agent + "_distance_to_" + zombie])
        episode.hist_data[agent + "_distance_to_" + zombie] = episode.user_data[agent + "_distance_to_" + zombie]
    for i in range(episode.user_data["n_zombies"]):
      agent = "zombie" + str(i+1)
      episode.custom_metrics[agent + "_explored_tiles"] = episode.user_data[agent + "_explored_tiles"]
      for j in range(episode.user_data["n_citizens"]):
        citizen = "citizen" + str(j+1)
        episode.custom_metrics[agent + "_distance_to_" + citizen] = np.mean(episode.user_data[agent + "_distance_to_" + citizen])
        episode.hist_data[agent + "_distance_to_" + citizen] = episode.user_data[agent + "_distance_to_" + citizen]

  def on_sample_end(self, worker: RolloutWorker, samples: SampleBatch,
                    **kwargs):
    pass

  def on_train_result(self, trainer, result: dict, **kwargs):
    result["callback_ok"] = True

  def on_postprocess_trajectory(self, worker: RolloutWorker,
                                episode: MultiAgentEpisode,
                                agent_id: str, policy_id: str,
                                policies: Dict[str, Policy],
                                postprocessed_batch: SampleBatch,
                                original_batches: Dict[str, SampleBatch], **kwargs):
    pass

