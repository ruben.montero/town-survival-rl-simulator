"""
Rubén Montero Vázquez. MUEI UDC 2019/20
"""

from ray.rllib.agents.ppo.ppo import PPOTrainer
from ray.rllib.agents.ppo.ppo_tf_policy import PPOTFPolicy
from ray.tune.logger import pretty_print
from ray.tune.registry import register_env
from town_zombies_multiagent_wrapper_env import TownZombiesMultiagentWrapperEnv
from training.no_move_policy import NoMovePolicy
from training.town_callbacks import TownZombiesCallbacks
from util.checkpoint_adapter import CheckpointAdapter
import os
import pickle

ENV_REGISTRATION_NAME = "TownSurvivalPPOPolicy"
CHECKPOINT_FREQ = 50
CHECKPOINT_ROOT_DIR = "checkpoint_ppo"
GRID_SEARCH_BATCH_SIZES = [4000, 8000, 16000]
GRID_SEARCH_CLIP_PARAMS = [0.05, 0.1, 0.2]
GRID_SEARCH_LEARNING_RATES = [0.0001, 0.0005, 0.001]

class PPOTF():

  def run_grid_search(self,
                      mode,
                      env_config,
                      n_iters,
                      checkpoint_base_dir,
                      zombie_checkpoint):
    for batch_size in GRID_SEARCH_BATCH_SIZES:
      for clip_param in GRID_SEARCH_CLIP_PARAMS:
        for lr in GRID_SEARCH_LEARNING_RATES:
          self.run_training(mode,
                            env_config,
                            n_iters,
                            batch_size,
                            clip_param,
                            lr,
                            checkpoint_base_dir,
                            zombie_checkpoint)

  def run_training(self,
                   mode,
                   env_config,
                   n_iters,
                   batch_size,
                   clip_param,
                   lr,
                   checkpoint_base_dir,
                   zombie_checkpoint):
    if checkpoint_base_dir is not None:
      checkpoint_dir = checkpoint_base_dir + "/" + CHECKPOINT_ROOT_DIR 
      checkpoint_dir += "/batchsize" + str(batch_size) + "_cp" + str(clip_param) + "_lr" + str(lr)
      os.makedirs(checkpoint_dir)
    if mode == "citizen":
      assert zombie_checkpoint is not None, "Missing zombie checkpoint.\
Training a citizen, so a checkpoint for the zombie must be provided with --zombie-checkpoint"
    env_registration_name = ENV_REGISTRATION_NAME 
    env_registration_name += ("Citizen" if mode == "citizen" else "Zombie")
    env_registration_name += "_batchsize" + str(batch_size) 
    env_registration_name += "_cp" + str(clip_param) + "_lr" + str(lr)
    register_env(env_registration_name, lambda _: TownZombiesMultiagentWrapperEnv(env_config))

    trainer = self.__create_trainer(mode,
                                    env_config,
                                    env_registration_name,
                                    batch_size,
                                    clip_param,
                                    lr,
                                    zombie_checkpoint)

    # Run training iterations
    for i in range(n_iters):
      print("== Iteration", i, "==")
      print(pretty_print(trainer.train()))
      # Checkpointing
      if checkpoint_base_dir is not None:
        if (i % CHECKPOINT_FREQ) == 0:
          print("Creating folder for checkpoint in iteration ", i)
          folder_name = checkpoint_dir + "/" + str(i)
          os.mkdir(folder_name)
          trainer.save(folder_name)

  # MARK:- Private

  def __create_trainer(self,
                       mode,
                       env_config,
                       env_registration_name,
                       batch_size,
                       clip_param,
                       lr,
                       zombie_checkpoint):
    obs_space = TownZombiesMultiagentWrapperEnv.observation_space()
    act_space = TownZombiesMultiagentWrapperEnv.action_space()
    policies = {
      "citizen1_policy": (PPOTFPolicy if mode == "citizen" else NoMovePolicy, obs_space, act_space, {}),
      "zombie1_policy": (PPOTFPolicy, obs_space, act_space, {})
    }
    trainer = PPOTrainer(
      env=env_registration_name,
      config={
        "multiagent": {
          "policies": policies,
          "policy_mapping_fn": lambda c: c + "_policy",
          "policies_to_train": ["citizen1_policy" if mode == "citizen" else "zombie1_policy"],
        },
        "batch_mode": "complete_episodes",
        "rollout_fragment_length": env_config["episode_steps"],
        "callbacks": TownZombiesCallbacks,
        "lr": lr,
        "train_batch_size": batch_size,
        "clip_param": clip_param
      })
    if mode == "citizen":
      self.__restore_checkpoint(trainer, zombie_checkpoint, obs_space, act_space)
    return trainer

  def __restore_checkpoint(self, trainer, zombie_checkpoint, obs_space, act_space):
    try:
      trainer.restore(zombie_checkpoint)
    except:
      print("An exception occured while attempting to restore zombie_checkpoint. \
This happens when using a freshly created checkpoint with --mode zombie, because \
zombies train against a citizen 'NoMovePolicy' and that can't be restored in a \
PPOTrainer prepared for training a citizen 'PPOTFPolicy'. Will try to adapt it...")
      adapter = CheckpointAdapter(obs_space, act_space)
      adapter.open("to_update", zombie_checkpoint)
      adapter.set_new_ppo_policy("to_update", "citizen1_policy")
      trainer.restore(zombie_checkpoint)
      print("Successfully restored checkpoint with trained zombie at " + zombie_checkpoint)
