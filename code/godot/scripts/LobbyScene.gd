extends Control

var line_edit_name: LineEdit
var line_edit_server: LineEdit
var line_edit_port: LineEdit
var line_edit_instance_number: LineEdit
var status_label: Label
var check_status_button: Button
var ready_toggle: CheckBox
var scroll_hbox_container: HBoxContainer

var socket = PacketPeerUDP.new()
var registered_as: String

func _ready():
	line_edit_name = get_node("PanelContainer/VBoxContainer/LineEditTag")
	line_edit_server = get_node("PanelContainer/VBoxContainer/LineEditServer")
	line_edit_port = get_node("PanelContainer/VBoxContainer/LineEditPort")
	line_edit_instance_number = get_node("PanelContainer/VBoxContainer/LineEditInstance")
	status_label = get_node("PanelContainer2/MessageLabel")
	check_status_button = get_node("PanelContainer/VBoxContainer/ButtonPing")
	ready_toggle= get_node("PanelContainer7/CheckBox")
	scroll_hbox_container = get_node("ScrollContainer/ScrollHBoxContainer")

func _process(_delta):
	if socket.is_listening():
		while socket.get_available_packet_count() > 0:
			var packet_str = socket.get_packet().get_string_from_utf8()
			print("Received " + packet_str)
			var parsed_packet = parse_packet_str(packet_str)
			if parsed_packet == null:
				print("Error with packet " + packet_str)
			elif parsed_packet is LobbyStatusDataframe:
				make_line_edits_not_editable()
				update_ui(parsed_packet)
				update_globals(parsed_packet)
				status_label.text = "Received update from server"
			elif parsed_packet is LobbyMessageDataframe:
				status_label.text = parsed_packet.message
			elif parsed_packet is LobbyRegisteredDataframe:
				registered_as = parsed_packet.registered
				ready_toggle.disabled = false
			elif parsed_packet is LobbyStartDataframe:
				socket.set_dest_address(socket.get_packet_ip(), parsed_packet.port)
				global.socket = socket
				global.registered_as = registered_as
				global.server_instance = _instance_number()
				get_tree().change_scene("res://MainScene.tscn")

# MARK:- Actions

func _on_ButtonPing_pressed():
	var ip = line_edit_server.text
	var port = line_edit_port.text
	socket.set_dest_address(ip, port.to_int())
	socket.put_packet(("zombies:" + _instance_number() + ";lobby").to_utf8())
	status_label.text = "Checking available lobbies in " + ip + ":" + port + "..."
	print("Sending 'zombies:lobby' to " + ip + ":" + port)

func _on_LineEditTag_text_changed(new_text):
	check_status_button.disabled = new_text.empty()

func _on_CheckBox_toggled(button_pressed):
	if registered_as == null:
		return
	var msg = "zombies:" + _instance_number() + ";ready=" + registered_as + ","
	var particle
	if button_pressed == true:
		particle = "true"
	else:
		particle = "false"
	msg += particle
	status_label.text = "Sending ready status update as " + particle + "..."
	socket.put_packet(msg.to_utf8())

# delegate from PanelContainerAgentTemplate
func _on_RegisterButton_pressed(agent_name):
	var msg = "zombies:" + _instance_number() + ";register=" + agent_name + ","
	msg += line_edit_name.text
	status_label.text = "Sending register request as" + agent_name + "..."
	socket.put_packet(msg.to_utf8())

# MARK:- Private

func make_line_edits_not_editable():
	line_edit_name.editable = false
	line_edit_server.editable = false
	line_edit_port.editable = false
	line_edit_instance_number.editable = false

func update_ui(dataframe: LobbyStatusDataframe):
	var panel_scene = load("res://custom_objects/PanelContainerAgentTemplate.tscn")
	for child in scroll_hbox_container.get_children():
		child.queue_free()
	for actor in dataframe.statuses:
		var panel_instance = panel_scene.instance()
		panel_instance.update_ui_actor(actor, self)
		scroll_hbox_container.add_child(panel_instance)

func update_globals(dataframe: LobbyStatusDataframe):
	global.n_citizens = 0
	global.n_zombies = 0
	for actor in dataframe.statuses:
		if actor.kind == "citizen":
			global.n_citizens += 1
		elif actor.kind == "zombie":
			global.n_zombies += 1

func parse_packet_str(packet_str):
	var components = packet_str.split(";")
	if components.size() < 2:
		print("General error parsing packet...")
		return null
	elif components.size() == 2:
		var subcomponents = components[1].split("=")
		if subcomponents.size() < 2:
			print("Error parsing packet...")
			return null
		elif subcomponents[0] == "message":
			return LobbyMessageDataframe.new(subcomponents[1])
		elif subcomponents[0] == "registered":
			return LobbyRegisteredDataframe.new(subcomponents[1])
		elif subcomponents[0] == "start":
			var info = subcomponents[1].split(":")
			if info.size() == 2:
				return LobbyStartDataframe.new(info[1])
			else:
				print("Error parsing packet...")
				return null
		else:
			print("Error parsing packet...")
			return null
	else:
		var status_datagram = LobbyStatusDataframe.new()
		for i in range(1, components.size()):
			var name_and_info = components[i].split("=")
			if name_and_info.size() != 2:
				print("Error parsing lobby packet. Malformed...")
				return null
			var info = name_and_info[1].split(",")
			if info.size() != 4:
				print("Error parsing lobby packet, incorrect info for agent...")
				return null
			status_datagram.statuses.append(LobbyActorStatus.new(
				name_and_info[0], info[0], info[1], info[2], info[3]))
		return status_datagram

func _instance_number():
	return line_edit_instance_number.text

class LobbyStatusDataframe:
	var statuses: Array # [LobbyActorStatus]
	func _init():
		self.statuses = []

class LobbyMessageDataframe:
	var message: String
	func _init(message: String):
		self.message = message

class LobbyRegisteredDataframe:
	var registered: String
	func _init(registered: String):
		self.registered = registered

class LobbyStartDataframe:
	var port: int
	func _init(port: String):
		self.port = port.to_int()

class LobbyActorStatus:
	var name: String # agent1
	var status: String # open | close
	var kind: String # citizen | zombie
	var tag: String  # Manuel
	var ready: bool

	func _init(name, status, kind, tag, ready):
		self.name = name
		self.status = status
		self.kind = kind
		self.tag = tag
		if ready == "ready":
			self.ready = true
		else:
			self.ready = false


func _on_ButtonBack_pressed():
	get_tree().change_scene("res://MenuScene.tscn")
