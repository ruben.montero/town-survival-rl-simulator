extends Node2D

var n_citizens: int
var n_zombies: int
var agent_index: int
var camera: Camera2D

var packet = null
var new_packet_flag = false

var _main_actor: Actor = null
var _other_actors = [] # [Actor]

const ANIM_DOWN = 0
const ANIM_LEFT = 1
const ANIM_RIGHT = 2
const ANIM_UP = 3

func _ready():
	n_citizens = global.n_citizens
	n_zombies = global.n_zombies
	agent_index = global.registered_as.trim_prefix("agent").to_int()
	initialize_sprites()

func _physics_process(delta):
	if packet == null:
		return
	if new_packet_flag == true:
		new_packet_flag = false
		render(packet)

func set_camera(camera):
	_main_actor.sprite.add_child(camera)
	camera.current = true

# MARK:- Private

func initialize_sprites():
	var citizen_scene = load("res://custom_objects/CitizenTemplate.tscn")
	var zombie_scene = load("res://custom_objects/ZombieTemplate.tscn")
	for i in range(n_citizens + n_zombies):
		var instance
		if i < n_citizens:
			print("adding citizen")
			instance = citizen_scene.instance()
		else:
			print("adding zombies")
			instance = zombie_scene.instance()
		if i == agent_index:
			_main_actor = Actor.new(instance)
		else:
			_other_actors.append(Actor.new(instance))
		add_child(instance)

func render(dataframe):
	# Nice-to-have: Interpolate render position
	var width  = 1024 # canvas size in scene
	var height = 1024 # canvas size in scene
	var other_agent = 0
	for i in range(n_citizens + n_zombies):
		var x = dataframe.all_coordinates[2*i] 
		var y = dataframe.all_coordinates[2*i+1]
		var actor_to_render: Actor
		if i == agent_index:
			actor_to_render = _main_actor
		else:
			actor_to_render = _other_actors[other_agent]
			other_agent += 1
		render_agent(actor_to_render, Vector2(x, y), width, height)

func render_agent(actor: Actor, normalized_pos: Vector2, w, h):
	var render_position = Vector2(w*normalized_pos.x, h*normalized_pos.y)
	var new_anim = new_animation_name(render_position, actor.last_position, actor.sprite)
	if new_anim != null:
		actor.sprite.animation = new_anim
	actor.sprite.position = render_position
	actor.last_position = render_position

func new_animation_name(position, old_position, node: AnimatedSprite):
	if old_position == null:
		return node.frames.get_animation_names()[ANIM_DOWN]
	else:
		var x_diff = position.x - old_position.x
		var y_diff = position.y - old_position.y
		return new_animation_name_for_delta(x_diff, y_diff, node)

func new_animation_name_for_delta(x_diff, y_diff, node):
	if abs(x_diff) > abs(y_diff):
		if x_diff > 0:
			return node.frames.get_animation_names()[ANIM_RIGHT]
		elif x_diff < 0:
			return node.frames.get_animation_names()[ANIM_LEFT]
		else:
			return null
	elif abs(x_diff) < abs(y_diff):
		if y_diff > 0:
			return node.frames.get_animation_names()[ANIM_DOWN]
		elif y_diff < 0:
			return node.frames.get_animation_names()[ANIM_UP]
		else:
			return null
	else:
		return null

class Actor:
	var sprite: AnimatedSprite
	var last_position: Vector2

	func _init(sprite: AnimatedSprite):
		self.sprite = sprite
