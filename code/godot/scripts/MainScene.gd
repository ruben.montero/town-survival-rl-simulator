extends Node2D

var socket = global.socket
var controller = null
var overlay: Node2D

func _ready():
	controller = get_node("AgentsController")
	overlay = get_node("OverlayHolder/Node2D")
	var camera: Camera2D = get_node("Camera2D")
	camera.socket = socket
	remove_child(camera)
	controller.set_camera(camera)
	overlay.camera = camera

func _physics_process(_delta):
	if socket.is_listening():
		while socket.get_available_packet_count() > 0:
			var dataframe = Dataframe.new(socket.get_packet().get_string_from_utf8())
			if dataframe != null:
				controller.packet = dataframe
				controller.new_packet_flag = true
				overlay.process_packet(dataframe)

func _exit_tree():
	socket.close()

# MARK:- Private

class Dataframe:
	var frame_index: int
	var is_done: bool
	var reward: float
	var observation: Array
	# Actual observation only contains information relative to self and
	# closer enemy. We use extra information to render all agents
	var all_coordinates: Array # x1,y1,x2,y2,...

	func _init(message_string: String):
		var components = message_string.split(";")
		if components.size() != 5:
			print("Malformed dataframe components. Skipping...")
			return null
		var header_components = components[0].split(":")
		if header_components.size() != 4:
			print("Malformed dataframe header. Skipping...")
			return null
		self.frame_index =  header_components[3].to_int()
		self.observation = []
		var obs_components = components[1].trim_prefix("obs=").split_floats(",")
		for obs_value in obs_components:
			observation.append(obs_value)
		self.reward = components[2].trim_prefix("reward=").to_float()
		self.is_done = components[3].trim_prefix("done=") == "true"
		self.all_coordinates = []
		var extra_components = components[4].trim_prefix("extra=").split_floats(",")
		for extra_value in extra_components:
			all_coordinates.append(extra_value)
