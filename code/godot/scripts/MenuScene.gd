extends Control

func _on_ButtonSimulation_pressed():
	get_tree().change_scene("res://LobbyScene.tscn")

func _on_ButtonExperimentsResults_pressed():
	get_tree().change_scene("res://TrainingResults.tscn")

func _on_ButtonHelp_pressed():
	get_tree().change_scene("res://WizardScene.tscn")

func _on_ButtonExit_pressed():
	# https://docs.godotengine.org/en/latest/tutorials/misc/handling_quit_requests.html
	get_tree().quit()
