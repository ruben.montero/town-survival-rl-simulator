extends Node2D

var socket: PacketPeerUDP = null

var current_action = Action.NO_MOVE

enum Action {
	NO_MOVE      = 0,
	MOVE_UP      = 1,
	MOVE_RIGHT   = 2,
	MOVE_DOWN    = 3,
	MOVE_LEFT    = 4,
}

func _physics_process(delta):
	if socket != null && update_current_action_if_changed():
		send_action_over_udp(current_action)

# MARK:- Private

func send_action_over_udp(action):
	socket.put_packet(("zombies:" + global.server_instance + ";action=" + str(action)).to_utf8())
	print("Sending action " + str(action))

# Returns true if a there was a change
func update_current_action_if_changed() -> bool:
	var up = check_action_changed_and_update_for("up")
	var right = check_action_changed_and_update_for("right")
	var down = check_action_changed_and_update_for("down")
	var left = check_action_changed_and_update_for("left")
	return up || right || down || left

func check_action_changed_and_update_for(key_name: String) -> bool:
	if Input.is_action_just_pressed(key_name):
		current_action = action_for_key_name(key_name)
		return true
	if Input.is_action_just_released(key_name):
		# e.g.: If pressed UP + RIGHT, and released RIGHT, ensure UP is recognised
		current_action = action_for_currently_pressed_keys()
		return true
	return false

func action_for_key_name(key_name: String):
	if key_name == "up":
		return Action.MOVE_UP
	elif key_name == "right":
		return Action.MOVE_RIGHT
	elif key_name == "down":
		return Action.MOVE_DOWN
	elif key_name == "left":
		return Action.MOVE_LEFT
	else:
		return Action.NO_MOVE

func action_for_currently_pressed_keys():
	if Input.is_action_pressed("up"):
		return Action.MOVE_UP
	elif Input.is_action_pressed("right"):
		return Action.MOVE_RIGHT
	elif Input.is_action_pressed("down"):
		return Action.MOVE_DOWN
	elif Input.is_action_pressed("left"):
		return Action.MOVE_LEFT
	else:
		return Action.NO_MOVE
