extends Node2D

var camera: Camera2D
var accumulated_reward = 0.0
var max_reward = 0.0
var episode_count = 1

func _process(_delta):
	if camera != null:
		self.position = camera.get_camera_screen_center()

func process_packet(dataframe):
	if dataframe.is_done:
		episode_count += 1
		accumulated_reward = 0
	accumulated_reward += dataframe.reward
	max_reward = max(max_reward, accumulated_reward)
	get_node("PanelExtra/Labels/LabelEpisode").text = str(episode_count)
	get_node("PanelExtra/Labels/LabelMax").text = str(stepify(max_reward, 0.001))
	get_node("PanelExtra/Labels/LabelReward").text = str(stepify(accumulated_reward, 0.001))
	get_node("PanelExtra/Labels/LabelStepCount").text = str(dataframe.frame_index)
	get_node("PanelObs/Labels/LabelTop").text = str(dataframe.observation[0])
	get_node("PanelObs/Labels/LabelRight").text = str(dataframe.observation[1])
	get_node("PanelObs/Labels/LabelBottom").text = str(dataframe.observation[2])
	get_node("PanelObs/Labels/LabelLeft").text = str(dataframe.observation[3])
	get_node("PanelObs/Labels/LabelSpeed").text = str(dataframe.observation[4])
	get_node("PanelObs/Labels/LabelX").text = str(dataframe.observation[5])
	get_node("PanelObs/Labels/LabelY").text = str(dataframe.observation[6])
	get_node("PanelWaiting").visible = false
	if dataframe.reward < 0:
		get_node("PanelExtra/Labels/LabelReward").modulate = Color(0.9, 0.2, 0.2)
	elif dataframe.reward < 0.99:
		get_node("PanelExtra/Labels/LabelReward").modulate = Color(0.8, 0.8, 0.1)
	else:
		get_node("PanelExtra/Labels/LabelReward").modulate = Color(0.2, 0.9, 0.2)

func _on_Button_pressed():
	get_tree().change_scene("res://MenuScene.tscn")
