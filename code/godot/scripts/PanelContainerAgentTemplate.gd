extends PanelContainer

var agent_name: String
var delegate: Node

func update_ui_actor(status, delegate):
	self.delegate = delegate
	self.agent_name = status.name
	var sprite: AnimatedSprite = get_node("AnimatedSprite")
	var name_label = get_node("VBoxContainer/NameLabel")
	var type_label = get_node("VBoxContainer/TypeLabel")
	var ready_label = get_node("VBoxContainer/ReadyLabel")
	var register_button = get_node("VBoxContainer/RegisterButton")
	name_label.text = status.tag
	type_label.text = "[" + status.status.to_upper() + "]"
	if status.kind == "citizen":
		sprite.frames = load("res://custom_objects/citizen_sprite.tres")
		sprite.animation = sprite.frames.get_animation_names()[0]
	else:
		sprite.frames = load("res://custom_objects/zombie_sprite.tres")
		sprite.animation = sprite.frames.get_animation_names()[0]
	register_button.text = "Register"
	register_button.disabled = status.status != "open"
	if status.ready == true:
		ready_label.add_color_override("font_color", Color(0.1, 0.9, 0.1))
		ready_label.text = "Ready"
	else:
		ready_label.add_color_override("font_color", Color(0.9, 0.1, 0.1))
		ready_label.text = "Not ready"

func _on_RegisterButton_pressed():
	delegate._on_RegisterButton_pressed(agent_name)
